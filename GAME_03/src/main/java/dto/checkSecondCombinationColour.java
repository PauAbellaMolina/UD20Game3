package dto;

public class checkSecondCombinationColour {

	static int combinationColour = 0;
	
	public checkSecondCombinationColour(int combinationColour) {
		
		this.combinationColour = combinationColour;
		
	}

	public static int getCombinationColour() {
		
		return combinationColour;
		
	}

	public static void setCombinationColour(int combinationColour) {
		
		checkSecondCombinationColour.combinationColour = combinationColour;
		
	}
	
}
