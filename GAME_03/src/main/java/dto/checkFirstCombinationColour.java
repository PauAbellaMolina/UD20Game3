package dto;

public class checkFirstCombinationColour {

	static int combinationColour = 0;
	
	public checkFirstCombinationColour(int combinationColour) {
		
		this.combinationColour = combinationColour;
		
	}

	public static int getCombinationColour() {
		
		return combinationColour;
		
	}

	public static void setCombinationColour(int combinationColour) {
		
		checkFirstCombinationColour.combinationColour = combinationColour;
		
	}
	
}
