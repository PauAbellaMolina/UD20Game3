package dto;

public class checkFourthCombinationColour {

	static int combinationColour = 0;
	
	public checkFourthCombinationColour(int combinationColour) {
		
		this.combinationColour = combinationColour;
		
	}

	public static int getCombinationColour() {
		
		return combinationColour;
		
	}

	public static void setCombinationColour(int combinationColour) {
		
		checkFourthCombinationColour.combinationColour = combinationColour;
		
	}
	
}
