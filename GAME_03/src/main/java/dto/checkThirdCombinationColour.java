package dto;

public class checkThirdCombinationColour {

	static int combinationColour = 0;
	
	public checkThirdCombinationColour(int combinationColour) {
		
		this.combinationColour = combinationColour;
		
	}

	public static int getCombinationColour() {
		
		return combinationColour;
		
	}

	public static void setCombinationColour(int combinationColour) {
		
		checkThirdCombinationColour.combinationColour = combinationColour;
		
	}
	
}
