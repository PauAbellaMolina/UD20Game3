package C2_UD20.GAME_03;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.SpringLayout;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JMenu;
import java.awt.Panel;
import javax.swing.JLabel;
import javax.swing.JInternalFrame;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;
import java.awt.Color;
import javax.swing.JButton;

import dto.*;

public class mainApp extends JFrame {

	private JPanel contentPane;

	public static void main(String[] args) {
		
		EventQueue.invokeLater(new Runnable() {
			
			public void run() {
				
				try {
					
					mainApp frame = new mainApp();
					frame.setVisible(true);
					
				} catch (Exception e) {
					
					e.printStackTrace();
					
				}
			}
			
		});
		
	}

	public mainApp() {
		setTitle("Hit and Blow");
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 580, 340);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBounds(0, 0, 684, 30);
		contentPane.add(menuBar);
		
		JMenu fileNewMenu = new JMenu("File");
		menuBar.add(fileNewMenu);
		
		JMenuItem newMenuItem = new JMenuItem("New Game");
		fileNewMenu.add(newMenuItem);
		
		JMenuItem exitMenuItem = new JMenuItem("Exit");
		fileNewMenu.add(exitMenuItem);
		
		JMenu helpNewMenu = new JMenu("Help");
		menuBar.add(helpNewMenu);
		
		JMenuItem howMenuItem = new JMenuItem("How to Play");
		helpNewMenu.add(howMenuItem);
		
		JMenuItem aboutMenuItem = new JMenuItem("About");
		helpNewMenu.add(aboutMenuItem);
		
		JPanel coloursPanel = new JPanel();
		coloursPanel.setBounds(354, 60, 200, 42);
		coloursPanel.setBorder(new LineBorder(new Color(192, 192, 192)));
		contentPane.add(coloursPanel);
		
		JLabel coloursLabel = new JLabel("Available Colours");
		coloursLabel.setBounds(354, 41, 110, 14);
		contentPane.add(coloursLabel);
		
		JLabel combinationLabel = new JLabel("Secret Combination");
		combinationLabel.setBounds(354, 116, 122, 14);
		contentPane.add(combinationLabel);
		
		JPanel combinationPanel = new JPanel();
		combinationPanel.setBounds(354, 135, 136, 42);
		coloursPanel.setLayout(null);
		
		final JButton firstColourButton = new JButton("");
		firstColourButton.setBounds(11, 11, 21, 21);
		coloursPanel.add(firstColourButton);
		
		final JButton secondColourButton = new JButton("");
		secondColourButton.setBounds(42, 11, 21, 21);
		coloursPanel.add(secondColourButton);
		
		final JButton thirdColourButton = new JButton("");
		thirdColourButton.setBounds(73, 11, 21, 21);
		coloursPanel.add(thirdColourButton);
		
		final JButton fourthColourButton = new JButton("");
		fourthColourButton.setBounds(104, 11, 21, 21);
		coloursPanel.add(fourthColourButton);
		
		final JButton fifthColourButton = new JButton("");
		fifthColourButton.setBounds(135, 11, 21, 21);
		coloursPanel.add(fifthColourButton);
		
		final JButton sixthColourButton = new JButton("");
		sixthColourButton.setBounds(166, 11, 21, 21);
		coloursPanel.add(sixthColourButton);
		combinationPanel.setBorder(new LineBorder(new Color(192, 192, 192)));
		contentPane.add(combinationPanel);
		combinationPanel.setLayout(null);
		
		final JButton secondCombinationButton = new JButton("");
		secondCombinationButton.setBounds(42, 11, 21, 21);
		combinationPanel.add(secondCombinationButton);
		
		final JButton thirdCombinationButton = new JButton("");
		thirdCombinationButton.setBounds(73, 11, 21, 21);
		combinationPanel.add(thirdCombinationButton);
		
		final JButton firstCombinationButton = new JButton("");
		firstCombinationButton.setBounds(11, 11, 21, 21);
		combinationPanel.add(firstCombinationButton);
		
		final JButton fourthCombinationButton = new JButton("");
		fourthCombinationButton.setBounds(104, 11, 21, 21);
		combinationPanel.add(fourthCombinationButton);
		
		JLabel triesLabel = new JLabel("Tries");
		triesLabel.setBounds(10, 41, 40, 14);
		contentPane.add(triesLabel);
		
		JPanel triesPanel = new JPanel();
		triesPanel.setBorder(new LineBorder(new Color(192, 192, 192)));
		triesPanel.setBounds(10, 60, 122, 225);
		contentPane.add(triesPanel);
		triesPanel.setLayout(null);
		
		final JButton triesColourButton_1_1 = new JButton("");
		triesColourButton_1_1.setBounds(10, 11, 21, 21);
		triesPanel.add(triesColourButton_1_1);
		
		final JButton triesColourButton_1_2 = new JButton("");
		triesColourButton_1_2.setBounds(37, 11, 21, 21);
		triesPanel.add(triesColourButton_1_2);
		
		final JButton triesColourButton_1_3 = new JButton("");
		triesColourButton_1_3.setBounds(64, 11, 21, 21);
		triesPanel.add(triesColourButton_1_3);
		
		final JButton triesColourButton_1_4 = new JButton("");
		triesColourButton_1_4.setBounds(91, 11, 21, 21);
		triesPanel.add(triesColourButton_1_4);
		
		final JButton triesColourButton_2_1 = new JButton("");
		triesColourButton_2_1.setBounds(10, 38, 21, 21);
		triesPanel.add(triesColourButton_2_1);
		
		final JButton triesColourButton_2_2 = new JButton("");
		triesColourButton_2_2.setBounds(37, 38, 21, 21);
		triesPanel.add(triesColourButton_2_2);
		
		final JButton triesColourButton_2_3 = new JButton("");
		triesColourButton_2_3.setBounds(64, 38, 21, 21);
		triesPanel.add(triesColourButton_2_3);
		
		final JButton triesColourButton_2_4 = new JButton("");
		triesColourButton_2_4.setBounds(91, 38, 21, 21);
		triesPanel.add(triesColourButton_2_4);
		
		final JButton triesColourButton_3_1 = new JButton("");
		triesColourButton_3_1.setBounds(10, 65, 21, 21);
		triesPanel.add(triesColourButton_3_1);
		
		final JButton triesColourButton_3_2 = new JButton("");
		triesColourButton_3_2.setBounds(37, 65, 21, 21);
		triesPanel.add(triesColourButton_3_2);
		
		final JButton triesColourButton_3_3 = new JButton("");
		triesColourButton_3_3.setBounds(64, 65, 21, 21);
		triesPanel.add(triesColourButton_3_3);
		
		final JButton triesColourButton_3_4 = new JButton("");
		triesColourButton_3_4.setBounds(91, 65, 21, 21);
		triesPanel.add(triesColourButton_3_4);
		
		final JButton triesColourButton_4_1 = new JButton("");
		triesColourButton_4_1.setBounds(10, 91, 21, 21);
		triesPanel.add(triesColourButton_4_1);
		
		final JButton triesColourButton_4_2 = new JButton("");
		triesColourButton_4_2.setBounds(37, 91, 21, 21);
		triesPanel.add(triesColourButton_4_2);
		
		final JButton triesColourButton_4_3 = new JButton("");
		triesColourButton_4_3.setBounds(64, 91, 21, 21);
		triesPanel.add(triesColourButton_4_3);
		
		final JButton triesColourButton_4_4 = new JButton("");
		triesColourButton_4_4.setBounds(91, 91, 21, 21);
		triesPanel.add(triesColourButton_4_4);
		
		final JButton triesColourButton_5_1 = new JButton("");
		triesColourButton_5_1.setBounds(10, 118, 21, 21);
		triesPanel.add(triesColourButton_5_1);
		
		final JButton triesColourButton_5_2 = new JButton("");
		triesColourButton_5_2.setBounds(37, 118, 21, 21);
		triesPanel.add(triesColourButton_5_2);
		
		final JButton triesColourButton_5_3 = new JButton("");
		triesColourButton_5_3.setBounds(64, 118, 21, 21);
		triesPanel.add(triesColourButton_5_3);
		
		final JButton triesColourButton_5_4 = new JButton("");
		triesColourButton_5_4.setBounds(91, 118, 21, 21);
		triesPanel.add(triesColourButton_5_4);
		
		final JButton triesColourButton_6_1 = new JButton("");
		triesColourButton_6_1.setBounds(10, 143, 21, 21);
		triesPanel.add(triesColourButton_6_1);
		
		final JButton triesColourButton_6_2 = new JButton("");
		triesColourButton_6_2.setBounds(37, 143, 21, 21);
		triesPanel.add(triesColourButton_6_2);
		
		final JButton triesColourButton_6_3 = new JButton("");
		triesColourButton_6_3.setBounds(64, 143, 21, 21);
		triesPanel.add(triesColourButton_6_3);
		
		final JButton triesColourButton_6_4 = new JButton("");
		triesColourButton_6_4.setBounds(91, 143, 21, 21);
		triesPanel.add(triesColourButton_6_4);
		
		final JButton triesColourButton_7_1 = new JButton("");
		triesColourButton_7_1.setBounds(10, 169, 21, 21);
		triesPanel.add(triesColourButton_7_1);
		
		final JButton triesColourButton_7_2 = new JButton("");
		triesColourButton_7_2.setBounds(37, 169, 21, 21);
		triesPanel.add(triesColourButton_7_2);
		
		final JButton triesColourButton_7_3 = new JButton("");
		triesColourButton_7_3.setBounds(64, 169, 21, 21);
		triesPanel.add(triesColourButton_7_3);
		
		final JButton triesColourButton_7_4 = new JButton("");
		triesColourButton_7_4.setBounds(91, 169, 21, 21);
		triesPanel.add(triesColourButton_7_4);
		
		final JButton triesColourButton_8_1 = new JButton("");
		triesColourButton_8_1.setBounds(10, 196, 21, 21);
		triesPanel.add(triesColourButton_8_1);
		
		final JButton triesColourButton_8_2 = new JButton("");
		triesColourButton_8_2.setBounds(37, 196, 21, 21);
		triesPanel.add(triesColourButton_8_2);
		
		final JButton triesColourButton_8_3 = new JButton("");
		triesColourButton_8_3.setBounds(64, 196, 21, 21);
		triesPanel.add(triesColourButton_8_3);
		
		final JButton triesColourButton_8_4 = new JButton("");
		triesColourButton_8_4.setBounds(91, 196, 21, 21);
		triesPanel.add(triesColourButton_8_4);
		
		final JButton checkButton_1 = new JButton("Check");
		checkButton_1.setEnabled(false);
		checkButton_1.setBounds(142, 71, 70, 21);
		contentPane.add(checkButton_1);
		
		JPanel hitsPanel = new JPanel();
		hitsPanel.setLayout(null);
		hitsPanel.setBorder(new LineBorder(new Color(192, 192, 192)));
		hitsPanel.setBounds(222, 60, 122, 225);
		contentPane.add(hitsPanel);
		
		final JButton hitsColourButton_1_1 = new JButton("");
		hitsColourButton_1_1.setBounds(10, 11, 21, 21);
		hitsPanel.add(hitsColourButton_1_1);
		
		final JButton hitsColourButton_1_2 = new JButton("");
		hitsColourButton_1_2.setBounds(37, 11, 21, 21);
		hitsPanel.add(hitsColourButton_1_2);
		
		final JButton hitsColourButton_1_3 = new JButton("");
		hitsColourButton_1_3.setBounds(64, 11, 21, 21);
		hitsPanel.add(hitsColourButton_1_3);
		
		final JButton hitsColourButton_1_4 = new JButton("");
		hitsColourButton_1_4.setBounds(91, 11, 21, 21);
		hitsPanel.add(hitsColourButton_1_4);
		
		final JButton hitsColourButton_2_1 = new JButton("");
		hitsColourButton_2_1.setBounds(10, 38, 21, 21);
		hitsPanel.add(hitsColourButton_2_1);
		
		final JButton hitsColourButton_2_2 = new JButton("");
		hitsColourButton_2_2.setBounds(37, 38, 21, 21);
		hitsPanel.add(hitsColourButton_2_2);
		
		final JButton hitsColourButton_2_3 = new JButton("");
		hitsColourButton_2_3.setBounds(64, 38, 21, 21);
		hitsPanel.add(hitsColourButton_2_3);
		
		final JButton hitsColourButton_2_4 = new JButton("");
		hitsColourButton_2_4.setBounds(91, 38, 21, 21);
		hitsPanel.add(hitsColourButton_2_4);
		
		final JButton hitsColourButton_3_1 = new JButton("");
		hitsColourButton_3_1.setBounds(10, 65, 21, 21);
		hitsPanel.add(hitsColourButton_3_1);
		
		final JButton hitsColourButton_3_2 = new JButton("");
		hitsColourButton_3_2.setBounds(37, 65, 21, 21);
		hitsPanel.add(hitsColourButton_3_2);
		
		final JButton hitsColourButton_3_3 = new JButton("");
		hitsColourButton_3_3.setBounds(64, 65, 21, 21);
		hitsPanel.add(hitsColourButton_3_3);
		
		final JButton hitsColourButton_3_4 = new JButton("");
		hitsColourButton_3_4.setBounds(91, 65, 21, 21);
		hitsPanel.add(hitsColourButton_3_4);
		
		final JButton hitsColourButton_4_1 = new JButton("");
		hitsColourButton_4_1.setBounds(10, 91, 21, 21);
		hitsPanel.add(hitsColourButton_4_1);
		
		final JButton hitsColourButton_4_2 = new JButton("");
		hitsColourButton_4_2.setBounds(37, 91, 21, 21);
		hitsPanel.add(hitsColourButton_4_2);
		
		final JButton hitsColourButton_4_3 = new JButton("");
		hitsColourButton_4_3.setBounds(64, 91, 21, 21);
		hitsPanel.add(hitsColourButton_4_3);
		
		final JButton hitsColourButton_4_4 = new JButton("");
		hitsColourButton_4_4.setBounds(91, 91, 21, 21);
		hitsPanel.add(hitsColourButton_4_4);
		
		final JButton hitsColourButton_5_1 = new JButton("");
		hitsColourButton_5_1.setBounds(10, 118, 21, 21);
		hitsPanel.add(hitsColourButton_5_1);
		
		final JButton hitsColourButton_5_2 = new JButton("");
		hitsColourButton_5_2.setBounds(37, 118, 21, 21);
		hitsPanel.add(hitsColourButton_5_2);
		
		final JButton hitsColourButton_5_3 = new JButton("");
		hitsColourButton_5_3.setBounds(64, 118, 21, 21);
		hitsPanel.add(hitsColourButton_5_3);
		
		final JButton hitsColourButton_5_4 = new JButton("");
		hitsColourButton_5_4.setBounds(91, 118, 21, 21);
		hitsPanel.add(hitsColourButton_5_4);
		
		final JButton hitsColourButton_6_1 = new JButton("");
		hitsColourButton_6_1.setBounds(10, 143, 21, 21);
		hitsPanel.add(hitsColourButton_6_1);
		
		final JButton hitsColourButton_6_2 = new JButton("");
		hitsColourButton_6_2.setBounds(37, 143, 21, 21);
		hitsPanel.add(hitsColourButton_6_2);
		
		final JButton hitsColourButton_6_3 = new JButton("");
		hitsColourButton_6_3.setBounds(64, 143, 21, 21);
		hitsPanel.add(hitsColourButton_6_3);
		
		final JButton hitsColourButton_6_4 = new JButton("");
		hitsColourButton_6_4.setBounds(91, 143, 21, 21);
		hitsPanel.add(hitsColourButton_6_4);
		
		final JButton hitsColourButton_7_1 = new JButton("");
		hitsColourButton_7_1.setBounds(10, 169, 21, 21);
		hitsPanel.add(hitsColourButton_7_1);
		
		final JButton hitsColourButton_7_2 = new JButton("");
		hitsColourButton_7_2.setBounds(37, 169, 21, 21);
		hitsPanel.add(hitsColourButton_7_2);
		
		final JButton hitsColourButton_7_3 = new JButton("");
		hitsColourButton_7_3.setBounds(64, 169, 21, 21);
		hitsPanel.add(hitsColourButton_7_3);
		
		final JButton hitsColourButton_7_4 = new JButton("");
		hitsColourButton_7_4.setBounds(91, 169, 21, 21);
		hitsPanel.add(hitsColourButton_7_4);
		
		final JButton hitsColourButton_8_1 = new JButton("");
		hitsColourButton_8_1.setBounds(10, 196, 21, 21);
		hitsPanel.add(hitsColourButton_8_1);
		
		final JButton hitsColourButton_8_2 = new JButton("");
		hitsColourButton_8_2.setBounds(37, 196, 21, 21);
		hitsPanel.add(hitsColourButton_8_2);
		
		final JButton hitsColourButton_8_3 = new JButton("");
		hitsColourButton_8_3.setBounds(64, 196, 21, 21);
		hitsPanel.add(hitsColourButton_8_3);
		
		final JButton hitsColourButton_8_4 = new JButton("");
		hitsColourButton_8_4.setBounds(91, 196, 21, 21);
		hitsPanel.add(hitsColourButton_8_4);
		
		JLabel hitsLabel = new JLabel("Hits and Blows");
		hitsLabel.setBounds(222, 41, 100, 14);
		contentPane.add(hitsLabel);
		
		final JButton checkButton_2 = new JButton("Check");
		checkButton_2.setEnabled(false);
		checkButton_2.setBounds(142, 98, 70, 21);
		contentPane.add(checkButton_2);
		
		final JButton checkButton_3 = new JButton("Check");
		checkButton_3.setEnabled(false);
		checkButton_3.setBounds(142, 125, 70, 21);
		contentPane.add(checkButton_3);
		
		final JButton checkButton_4 = new JButton("Check");
		checkButton_4.setEnabled(false);
		checkButton_4.setBounds(142, 151, 70, 21);
		contentPane.add(checkButton_4);
		
		final JButton checkButton_5 = new JButton("Check");
		checkButton_5.setEnabled(false);
		checkButton_5.setBounds(142, 178, 70, 21);
		contentPane.add(checkButton_5);
		
		final JButton checkButton_6 = new JButton("Check");
		checkButton_6.setEnabled(false);
		checkButton_6.setBounds(142, 203, 70, 21);
		contentPane.add(checkButton_6);
		
		final JButton checkButton_7 = new JButton("Check");
		checkButton_7.setEnabled(false);
		checkButton_7.setBounds(142, 229, 70, 21);
		contentPane.add(checkButton_7);
		
		final JButton checkButton_8 = new JButton("Check");
		checkButton_8.setEnabled(false);
		checkButton_8.setBounds(142, 256, 70, 21);
		contentPane.add(checkButton_8);
		
		JLabel legendLabel = new JLabel("Legend");
		legendLabel.setBounds(354, 188, 122, 14);
		contentPane.add(legendLabel);
		
		JPanel legendPanel = new JPanel();
		legendPanel.setLayout(null);
		legendPanel.setBorder(new LineBorder(new Color(192, 192, 192)));
		legendPanel.setBounds(354, 207, 163, 78);
		contentPane.add(legendPanel);
		
		final JButton blowLegendButton = new JButton("");
		blowLegendButton.setBounds(11, 46, 21, 21);
		legendPanel.add(blowLegendButton);
		
		final JButton hitLegendButton = new JButton("");
		hitLegendButton.setBounds(11, 11, 21, 21);
		legendPanel.add(hitLegendButton);
		
		JLabel legendHitLabel = new JLabel("Hit: Color + Position");
		legendHitLabel.setBounds(42, 14, 120, 14);
		legendPanel.add(legendHitLabel);
		
		JLabel legendBlowLabel = new JLabel("Blow: Color Only");
		legendBlowLabel.setBounds(42, 49, 120, 14);
		legendPanel.add(legendBlowLabel);
		
		newMenuItem.addActionListener(new ActionListener() {
			
			public void actionPerformed (ActionEvent e) {

				Random randomGenerator = new Random();
				
				// Posicionamineto de los colores disponibles
				firstColourButton.setBackground(Color.BLUE);
				secondColourButton.setBackground(Color.RED);
				thirdColourButton.setBackground(Color.GREEN);
				fourthColourButton.setBackground(Color.YELLOW);
				fifthColourButton.setBackground(Color.MAGENTA);
				sixthColourButton.setBackground(Color.LIGHT_GRAY);
				
				// Posicionamineto de los colores de la leyenda
				hitLegendButton.setBackground(Color.ORANGE);
				blowLegendButton.setBackground(Color.WHITE);
				
				// Reseteo de todos los aciertos
				checkFirstCombinationColour.setCombinationColour(0);
				checkSecondCombinationColour.setCombinationColour(0);
				checkThirdCombinationColour.setCombinationColour(0);
				checkFourthCombinationColour.setCombinationColour(0);
				
				checkFirstTryColour.setTryColour(0);
				checkSecondTryColour.setTryColour(0);
				checkThirdTryColour.setTryColour(0);
				checkFourthTryColour.setTryColour(0);
				
				for (int i = 0; i < 6; i++) {
					
					// Creación de la combinación secreta
					int randomCombinationColour = randomGenerator.nextInt(6);
					
					if (randomCombinationColour == 0 && i == 0) {
						
						secondCombinationButton.setBackground(Color.BLUE);
						checkSecondCombinationColour.setCombinationColour(1);
						
					} else if (randomCombinationColour == 1 && i == 0) {
						
						secondCombinationButton.setBackground(Color.RED);
						checkSecondCombinationColour.setCombinationColour(2);
						
					} else if (randomCombinationColour == 2 && i == 0) {
						
						secondCombinationButton.setBackground(Color.GREEN);
						checkSecondCombinationColour.setCombinationColour(3);
						
					} else if (randomCombinationColour == 3 && i == 0) {
						
						secondCombinationButton.setBackground(Color.YELLOW);
						checkSecondCombinationColour.setCombinationColour(4);
						
					} else if (randomCombinationColour == 4 && i == 0) {
						
						secondCombinationButton.setBackground(Color.MAGENTA);
						checkSecondCombinationColour.setCombinationColour(5);
						
					} else if (randomCombinationColour == 5 && i == 0) {
						
						secondCombinationButton.setBackground(Color.LIGHT_GRAY);
						checkSecondCombinationColour.setCombinationColour(6);
						
					}
					
					if (randomCombinationColour == 0 && i == 1) {
						
						thirdCombinationButton.setBackground(Color.BLUE);
						checkThirdCombinationColour.setCombinationColour(1);
						
					} else if (randomCombinationColour == 1 && i == 1) {
						
						thirdCombinationButton.setBackground(Color.RED);
						checkThirdCombinationColour.setCombinationColour(2);
						
					} else if (randomCombinationColour == 2 && i == 1) {
						
						thirdCombinationButton.setBackground(Color.GREEN);
						checkThirdCombinationColour.setCombinationColour(3);
						
					} else if (randomCombinationColour == 3 && i == 1) {
						
						thirdCombinationButton.setBackground(Color.YELLOW);
						checkThirdCombinationColour.setCombinationColour(4);
						
					} else if (randomCombinationColour == 4 && i == 1) {
						
						thirdCombinationButton.setBackground(Color.MAGENTA);
						checkThirdCombinationColour.setCombinationColour(5);
						
					} else if (randomCombinationColour == 5 && i == 1) {
						
						thirdCombinationButton.setBackground(Color.LIGHT_GRAY);
						checkThirdCombinationColour.setCombinationColour(6);
						
					}
					
					if (randomCombinationColour == 0 && i == 2) {
						
						firstCombinationButton.setBackground(Color.BLUE);
						checkFirstCombinationColour.setCombinationColour(1);
						
					} else if (randomCombinationColour == 1 && i == 2) {
						
						firstCombinationButton.setBackground(Color.RED);
						checkFirstCombinationColour.setCombinationColour(2);
						
					} else if (randomCombinationColour == 2 && i == 2) {
						
						firstCombinationButton.setBackground(Color.GREEN);
						checkFirstCombinationColour.setCombinationColour(3);
						
					} else if (randomCombinationColour == 3 && i == 2) {
						
						firstCombinationButton.setBackground(Color.YELLOW);
						checkFirstCombinationColour.setCombinationColour(4);
						
					} else if (randomCombinationColour == 4 && i == 2) {
						
						firstCombinationButton.setBackground(Color.MAGENTA);
						checkFirstCombinationColour.setCombinationColour(5);
						
					} else if (randomCombinationColour == 5 && i == 2) {
						
						firstCombinationButton.setBackground(Color.LIGHT_GRAY);
						checkFirstCombinationColour.setCombinationColour(6);
						
					}
					
					if (randomCombinationColour == 0 && i == 3) {
						
						fourthCombinationButton.setBackground(Color.BLUE);
						checkFourthCombinationColour.setCombinationColour(1);
						
					} else if (randomCombinationColour == 1 && i == 3) {
						
						fourthCombinationButton.setBackground(Color.RED);
						checkFourthCombinationColour.setCombinationColour(2);
						
					} else if (randomCombinationColour == 2 && i == 3) {
						
						fourthCombinationButton.setBackground(Color.GREEN);
						checkFourthCombinationColour.setCombinationColour(3);
						
					} else if (randomCombinationColour == 3 && i == 3) {
						
						fourthCombinationButton.setBackground(Color.YELLOW);
						checkFourthCombinationColour.setCombinationColour(4);
						
					} else if (randomCombinationColour == 4 && i == 3) {
						
						fourthCombinationButton.setBackground(Color.MAGENTA);
						checkFourthCombinationColour.setCombinationColour(5);
						
					} else if (randomCombinationColour == 5 && i == 3) {
						
						fourthCombinationButton.setBackground(Color.LIGHT_GRAY);
						checkFourthCombinationColour.setCombinationColour(6);
						
					}
					
				}
				
				// Reseteo de todos los botones
				triesColourButton_1_1.setBackground(null);
				triesColourButton_1_2.setBackground(null);
				triesColourButton_1_3.setBackground(null);
				triesColourButton_1_4.setBackground(null);
				triesColourButton_2_1.setBackground(null);
				triesColourButton_2_2.setBackground(null);
				triesColourButton_2_3.setBackground(null);
				triesColourButton_2_4.setBackground(null);
				triesColourButton_3_1.setBackground(null);
				triesColourButton_3_2.setBackground(null);
				triesColourButton_3_3.setBackground(null);
				triesColourButton_3_4.setBackground(null);
				triesColourButton_4_1.setBackground(null);
				triesColourButton_4_2.setBackground(null);
				triesColourButton_4_3.setBackground(null);
				triesColourButton_4_4.setBackground(null);
				triesColourButton_5_1.setBackground(null);
				triesColourButton_5_2.setBackground(null);
				triesColourButton_5_3.setBackground(null);
				triesColourButton_5_4.setBackground(null);
				triesColourButton_6_1.setBackground(null);
				triesColourButton_6_2.setBackground(null);
				triesColourButton_6_3.setBackground(null);
				triesColourButton_6_4.setBackground(null);
				triesColourButton_7_1.setBackground(null);
				triesColourButton_7_2.setBackground(null);
				triesColourButton_7_3.setBackground(null);
				triesColourButton_7_4.setBackground(null);
				triesColourButton_8_1.setBackground(null);
				triesColourButton_8_2.setBackground(null);
				triesColourButton_8_3.setBackground(null);
				triesColourButton_8_4.setBackground(null);
				
				checkButton_1.setEnabled(true);
				checkButton_2.setEnabled(false);
				checkButton_3.setEnabled(false);
				checkButton_4.setEnabled(false);
				checkButton_5.setEnabled(false);
				checkButton_6.setEnabled(false);
				checkButton_7.setEnabled(false);
				checkButton_8.setEnabled(false);
				
				hitsColourButton_1_1.setBackground(null);
				hitsColourButton_1_2.setBackground(null);
				hitsColourButton_1_3.setBackground(null);
				hitsColourButton_1_4.setBackground(null);
				hitsColourButton_2_1.setBackground(null);
				hitsColourButton_2_2.setBackground(null);
				hitsColourButton_2_3.setBackground(null);
				hitsColourButton_2_4.setBackground(null);
				hitsColourButton_3_1.setBackground(null);
				hitsColourButton_3_2.setBackground(null);
				hitsColourButton_3_3.setBackground(null);
				hitsColourButton_3_4.setBackground(null);
				hitsColourButton_4_1.setBackground(null);
				hitsColourButton_4_2.setBackground(null);
				hitsColourButton_4_3.setBackground(null);
				hitsColourButton_4_4.setBackground(null);
				hitsColourButton_5_1.setBackground(null);
				hitsColourButton_5_2.setBackground(null);
				hitsColourButton_5_3.setBackground(null);
				hitsColourButton_5_4.setBackground(null);
				hitsColourButton_6_1.setBackground(null);
				hitsColourButton_6_2.setBackground(null);
				hitsColourButton_6_3.setBackground(null);
				hitsColourButton_6_4.setBackground(null);
				hitsColourButton_7_1.setBackground(null);
				hitsColourButton_7_2.setBackground(null);
				hitsColourButton_7_3.setBackground(null);
				hitsColourButton_7_4.setBackground(null);
				hitsColourButton_8_1.setBackground(null);
				hitsColourButton_8_2.setBackground(null);
				hitsColourButton_8_3.setBackground(null);
				hitsColourButton_8_4.setBackground(null);
				
				// ActionListener para cada botón de la zona de intentos
				triesColourButton_1_1.addActionListener(new ActionListener() {
					
					int counter = 0;
					
					public void actionPerformed (ActionEvent e) {
						
						counter++;
						
						if (counter == 1) {
							
							triesColourButton_1_1.setBackground(Color.BLUE);
							checkFirstTryColour.setTryColour(1);
							
						} else if (counter == 2) {
							
							triesColourButton_1_1.setBackground(Color.RED);
							checkFirstTryColour.setTryColour(2);
							
						} else if (counter == 3) {
							
							triesColourButton_1_1.setBackground(Color.GREEN);
							checkFirstTryColour.setTryColour(3);
							
						} else if (counter == 4) {
							
							triesColourButton_1_1.setBackground(Color.YELLOW);
							checkFirstTryColour.setTryColour(4);
							
						} else if (counter == 5) {
							
							triesColourButton_1_1.setBackground(Color.MAGENTA);
							checkFirstTryColour.setTryColour(5);
							
						} else if (counter == 6) {
							
							triesColourButton_1_1.setBackground(Color.LIGHT_GRAY);
							checkFirstTryColour.setTryColour(6);
							counter = 0;
							
						}
						
					}
					
				});
				
				triesColourButton_1_2.addActionListener(new ActionListener() {
					
					int counter = 0;
					
					public void actionPerformed (ActionEvent e) {
						
						counter++;
						
						if (counter == 1) {
							
							triesColourButton_1_2.setBackground(Color.BLUE);
							checkSecondTryColour.setTryColour(1);
							
						} else if (counter == 2) {
							
							triesColourButton_1_2.setBackground(Color.RED);
							checkSecondTryColour.setTryColour(2);
							
						} else if (counter == 3) {
							
							triesColourButton_1_2.setBackground(Color.GREEN);
							checkSecondTryColour.setTryColour(3);
							
						} else if (counter == 4) {
							
							triesColourButton_1_2.setBackground(Color.YELLOW);
							checkSecondTryColour.setTryColour(4);
							
						} else if (counter == 5) {
							
							triesColourButton_1_2.setBackground(Color.MAGENTA);
							checkSecondTryColour.setTryColour(5);
							
						} else if (counter == 6) {
							
							triesColourButton_1_2.setBackground(Color.LIGHT_GRAY);
							checkSecondTryColour.setTryColour(6);
							counter = 0;
							
						}
						
					}
					
				});
				
				triesColourButton_1_3.addActionListener(new ActionListener() {
					
					int counter = 0;
					
					public void actionPerformed (ActionEvent e) {
						
						counter++;
						
						if (counter == 1) {
							
							triesColourButton_1_3.setBackground(Color.BLUE);
							checkThirdTryColour.setTryColour(1);
							
						} else if (counter == 2) {
							
							triesColourButton_1_3.setBackground(Color.RED);
							checkThirdTryColour.setTryColour(2);
							
						} else if (counter == 3) {
							
							triesColourButton_1_3.setBackground(Color.GREEN);
							checkThirdTryColour.setTryColour(3);
							
						} else if (counter == 4) {
							
							triesColourButton_1_3.setBackground(Color.YELLOW);
							checkThirdTryColour.setTryColour(4);
							
						} else if (counter == 5) {
							
							triesColourButton_1_3.setBackground(Color.MAGENTA);
							checkThirdTryColour.setTryColour(5);
							
						} else if (counter == 6) {
							
							triesColourButton_1_3.setBackground(Color.LIGHT_GRAY);
							checkThirdTryColour.setTryColour(6);
							counter = 0;
							
						}
						
					}
					
				});
				
				triesColourButton_1_4.addActionListener(new ActionListener() {
					
					int counter = 0;
					
					public void actionPerformed (ActionEvent e) {
						
						counter++;
						
						if (counter == 1) {
							
							triesColourButton_1_4.setBackground(Color.BLUE);
							checkFourthTryColour.setTryColour(1);
							
						} else if (counter == 2) {
							
							triesColourButton_1_4.setBackground(Color.RED);
							checkFourthTryColour.setTryColour(2);
							
						} else if (counter == 3) {
							
							triesColourButton_1_4.setBackground(Color.GREEN);
							checkFourthTryColour.setTryColour(3);
							
						} else if (counter == 4) {
							
							triesColourButton_1_4.setBackground(Color.YELLOW);
							checkFourthTryColour.setTryColour(4);
							
						} else if (counter == 5) {
							
							triesColourButton_1_4.setBackground(Color.MAGENTA);
							checkFourthTryColour.setTryColour(5);
							
						} else if (counter == 6) {
							
							triesColourButton_1_4.setBackground(Color.LIGHT_GRAY);
							checkFourthTryColour.setTryColour(6);
							counter = 0;
							
						}
						
					}

				});
				
				checkButton_1.addActionListener(new ActionListener() {
					
					public void actionPerformed (ActionEvent e) {
						
						checkButton_1.setEnabled(false);
						checkButton_2.setEnabled(true);
						
						// Comparación entre los colores de "Tries" y "Hits and Blows"
						int countHits = 0;
						int countBlows = 0;
						
						if (checkFirstCombinationColour.getCombinationColour() == 1 && checkFirstTryColour.getTryColour() == 1) {
							
							hitsColourButton_1_1.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 1 && checkFirstTryColour.getTryColour() == 1) {
							
							hitsColourButton_1_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 1 && checkFirstTryColour.getTryColour() == 1) {
							
							hitsColourButton_1_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 1 && checkFirstTryColour.getTryColour() == 1) {
							
							hitsColourButton_1_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 2 && checkFirstTryColour.getTryColour() == 2) {
							
							hitsColourButton_1_1.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 2 && checkFirstTryColour.getTryColour() == 2) {
							
							hitsColourButton_1_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 2 && checkFirstTryColour.getTryColour() == 2) {
							
							hitsColourButton_1_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 2 && checkFirstTryColour.getTryColour() == 2) {
							
							hitsColourButton_1_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 3 && checkFirstTryColour.getTryColour() == 3) {
							
							hitsColourButton_1_1.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 3 && checkFirstTryColour.getTryColour() == 3) {
							
							hitsColourButton_1_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 3 && checkFirstTryColour.getTryColour() == 3) {
							
							hitsColourButton_1_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 3 && checkFirstTryColour.getTryColour() == 3) {
							
							hitsColourButton_1_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 4 && checkFirstTryColour.getTryColour() == 4) {
							
							hitsColourButton_1_1.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 4 && checkFirstTryColour.getTryColour() == 4) {
							
							hitsColourButton_1_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 4 && checkFirstTryColour.getTryColour() == 4) {
							
							hitsColourButton_1_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 4 && checkFirstTryColour.getTryColour() == 4) {
							
							hitsColourButton_1_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 5 && checkFirstTryColour.getTryColour() == 5) {
							
							hitsColourButton_1_1.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 5 && checkFirstTryColour.getTryColour() == 5) {
							
							hitsColourButton_1_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 5 && checkFirstTryColour.getTryColour() == 5) {
							
							hitsColourButton_1_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 5 && checkFirstTryColour.getTryColour() == 5) {
							
							hitsColourButton_1_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 6 && checkFirstTryColour.getTryColour() == 6) {
							
							hitsColourButton_1_1.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 6 && checkFirstTryColour.getTryColour() == 6) {
							
							hitsColourButton_1_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 6 && checkFirstTryColour.getTryColour() == 6) {
							
							hitsColourButton_1_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 6 && checkFirstTryColour.getTryColour() == 6) {
							
							hitsColourButton_1_1.setBackground(Color.WHITE);
							countBlows++;
							
						}
						
						if (checkSecondCombinationColour.getCombinationColour() == 1 && checkSecondTryColour.getTryColour() == 1) {
							
							hitsColourButton_1_2.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 1 && checkSecondTryColour.getTryColour() == 1) {
							
							hitsColourButton_1_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 1 && checkSecondTryColour.getTryColour() == 1) {
							
							hitsColourButton_1_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 1 && checkSecondTryColour.getTryColour() == 1) {
							
							hitsColourButton_1_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 2 && checkSecondTryColour.getTryColour() == 2) {
							
							hitsColourButton_1_2.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 2 && checkSecondTryColour.getTryColour() == 2) {
							
							hitsColourButton_1_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 2 && checkSecondTryColour.getTryColour() == 2) {
							
							hitsColourButton_1_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 2 && checkSecondTryColour.getTryColour() == 2) {
							
							hitsColourButton_1_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 3 && checkSecondTryColour.getTryColour() == 3) {
							
							hitsColourButton_1_2.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 3 && checkSecondTryColour.getTryColour() == 3) {
							
							hitsColourButton_1_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 3 && checkSecondTryColour.getTryColour() == 3) {
							
							hitsColourButton_1_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 3 && checkSecondTryColour.getTryColour() == 3) {
							
							hitsColourButton_1_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 4 && checkSecondTryColour.getTryColour() == 4) {
							
							hitsColourButton_1_2.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 4 && checkSecondTryColour.getTryColour() == 4) {
							
							hitsColourButton_1_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 4 && checkSecondTryColour.getTryColour() == 4) {
							
							hitsColourButton_1_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 4 && checkSecondTryColour.getTryColour() == 4) {
							
							hitsColourButton_1_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 5 && checkSecondTryColour.getTryColour() == 5) {
							
							hitsColourButton_1_2.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 5 && checkSecondTryColour.getTryColour() == 5) {
							
							hitsColourButton_1_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 5 && checkSecondTryColour.getTryColour() == 5) {
							
							hitsColourButton_1_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 5 && checkSecondTryColour.getTryColour() == 5) {
							
							hitsColourButton_1_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 6 && checkSecondTryColour.getTryColour() == 6) {
							
							hitsColourButton_1_2.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 6 && checkSecondTryColour.getTryColour() == 6) {
							
							hitsColourButton_1_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 6 && checkSecondTryColour.getTryColour() == 6) {
							
							hitsColourButton_1_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 6 && checkSecondTryColour.getTryColour() == 6) {
							
							hitsColourButton_1_2.setBackground(Color.WHITE);
							countBlows++;
							
						}
						
						if (checkThirdCombinationColour.getCombinationColour() == 1 && checkThirdTryColour.getTryColour() == 1) {
							
							hitsColourButton_1_3.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 1 && checkThirdTryColour.getTryColour() == 1) {
							
							hitsColourButton_1_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 1 && checkThirdTryColour.getTryColour() == 1) {
							
							hitsColourButton_1_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 1 && checkThirdTryColour.getTryColour() == 1) {
							
							hitsColourButton_1_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 2 && checkThirdTryColour.getTryColour() == 2) {
							
							hitsColourButton_1_3.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 2 && checkThirdTryColour.getTryColour() == 2) {
							
							hitsColourButton_1_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 2 && checkThirdTryColour.getTryColour() == 2) {
							
							hitsColourButton_1_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 2 && checkThirdTryColour.getTryColour() == 2) {
							
							hitsColourButton_1_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 3 && checkThirdTryColour.getTryColour() == 3) {
							
							hitsColourButton_1_3.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 3 && checkThirdTryColour.getTryColour() == 3) {
							
							hitsColourButton_1_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 3 && checkThirdTryColour.getTryColour() == 3) {
							
							hitsColourButton_1_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 3 && checkThirdTryColour.getTryColour() == 3) {
							
							hitsColourButton_1_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 4 && checkThirdTryColour.getTryColour() == 4) {
							
							hitsColourButton_1_3.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 4 && checkThirdTryColour.getTryColour() == 4) {
							
							hitsColourButton_1_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 4 && checkThirdTryColour.getTryColour() == 4) {
							
							hitsColourButton_1_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 4 && checkThirdTryColour.getTryColour() == 4) {
							
							hitsColourButton_1_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 5 && checkThirdTryColour.getTryColour() == 5) {
							
							hitsColourButton_1_3.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 5 && checkThirdTryColour.getTryColour() == 5) {
							
							hitsColourButton_1_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 5 && checkThirdTryColour.getTryColour() == 5) {
							
							hitsColourButton_1_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 5 && checkThirdTryColour.getTryColour() == 5) {
							
							hitsColourButton_1_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 6 && checkThirdTryColour.getTryColour() == 6) {
							
							hitsColourButton_1_3.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 6 && checkThirdTryColour.getTryColour() == 6) {
							
							hitsColourButton_1_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 6 && checkThirdTryColour.getTryColour() == 6) {
							
							hitsColourButton_1_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 6 && checkThirdTryColour.getTryColour() == 6) {
							
							hitsColourButton_1_3.setBackground(Color.WHITE);
							countBlows++;
							
						}
						
						if (checkFourthCombinationColour.getCombinationColour() == 1 && checkFourthTryColour.getTryColour() == 1) {
							
							hitsColourButton_1_4.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 1 && checkFourthTryColour.getTryColour() == 1) {
							
							hitsColourButton_1_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 1 && checkFourthTryColour.getTryColour() == 1) {
							
							hitsColourButton_1_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 1 && checkFourthTryColour.getTryColour() == 1) {
							
							hitsColourButton_1_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 2 && checkFourthTryColour.getTryColour() == 2) {
							
							hitsColourButton_1_4.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 2 && checkFourthTryColour.getTryColour() == 2) {
							
							hitsColourButton_1_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 2 && checkFourthTryColour.getTryColour() == 2) {
							
							hitsColourButton_1_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 2 && checkFourthTryColour.getTryColour() == 2) {
							
							hitsColourButton_1_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 3 && checkFourthTryColour.getTryColour() == 3) {
							
							hitsColourButton_1_4.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 3 && checkFourthTryColour.getTryColour() == 3) {
							
							hitsColourButton_1_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 3 && checkFourthTryColour.getTryColour() == 3) {
							
							hitsColourButton_1_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 3 && checkFourthTryColour.getTryColour() == 3) {
							
							hitsColourButton_1_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 4 && checkFourthTryColour.getTryColour() == 4) {
							
							hitsColourButton_1_4.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 4 && checkFourthTryColour.getTryColour() == 4) {
							
							hitsColourButton_1_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 4 && checkFourthTryColour.getTryColour() == 4) {
							
							hitsColourButton_1_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 4 && checkFourthTryColour.getTryColour() == 4) {
							
							hitsColourButton_1_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 5 && checkFourthTryColour.getTryColour() == 5) {
							
							hitsColourButton_1_4.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 5 && checkFourthTryColour.getTryColour() == 5) {
							
							hitsColourButton_1_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 5 && checkFourthTryColour.getTryColour() == 5) {
							
							hitsColourButton_1_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 5 && checkFourthTryColour.getTryColour() == 5) {
							
							hitsColourButton_1_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 6 && checkFourthTryColour.getTryColour() == 6) {
							
							hitsColourButton_1_4.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 6 && checkFourthTryColour.getTryColour() == 6) {
							
							hitsColourButton_1_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 6 && checkFourthTryColour.getTryColour() == 6) {
							
							hitsColourButton_1_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 6 && checkFourthTryColour.getTryColour() == 6) {
							
							hitsColourButton_1_4.setBackground(Color.WHITE);
							countBlows++;
							
						}
						
						if (countHits > 1 && countHits < 4 && countBlows > 1) {
							
							JOptionPane.showMessageDialog(null, countHits + " Hits! " + countBlows + " Blows!");
							
						} else if (countHits > 1 && countHits < 4 && countBlows == 1) {
							
							JOptionPane.showMessageDialog(null, countHits + " Hits! " + countBlows + " Blow!");
							
						} else if (countHits == 1 && countBlows > 1) {
							
							JOptionPane.showMessageDialog(null, countHits + " Hit! " + countBlows + " Blows!");
							
						} else if (countHits == 1 && countBlows == 1) {
							
							JOptionPane.showMessageDialog(null, countHits + " Hit! " + countBlows + " Blow!");
							
						} else if (countHits > 1 && countHits < 4 && countBlows < 1) {
							
							JOptionPane.showMessageDialog(null, countHits + " Hits! " + countBlows + " Blows!");
							
						} else if (countHits == 1 && countBlows < 1) {
							
							JOptionPane.showMessageDialog(null, countHits + " Hit! " + countBlows + " Blows!");
							
						} else if (countHits < 1 && countBlows > 1) {
							
							JOptionPane.showMessageDialog(null, countHits + " Hits! " + countBlows + " Blows!");
							
						} else if (countHits < 1 && countBlows == 1) {
							
							JOptionPane.showMessageDialog(null, countHits + " Hits! " + countBlows + " Blow!");
							
						} else if (countHits < 1 && countBlows < 1) {
							
							JOptionPane.showMessageDialog(null, countHits + " Hits! " + countBlows + " Blows!");
							
						} else if (countHits == 4) {
							
							checkButton_2.setEnabled(false);
							
							JOptionPane.showMessageDialog(null, "You won the game!");
							
						}
						
						triesColourButton_2_1.addActionListener(new ActionListener() {
							
							int counter = 0;
							
							public void actionPerformed (ActionEvent e) {
								
								counter++;
								
								if (counter == 1) {
									
									triesColourButton_2_1.setBackground(Color.BLUE);
									checkFirstTryColour.setTryColour(1);
									
								} else if (counter == 2) {
									
									triesColourButton_2_1.setBackground(Color.RED);
									checkFirstTryColour.setTryColour(2);
									
								} else if (counter == 3) {
									
									triesColourButton_2_1.setBackground(Color.GREEN);
									checkFirstTryColour.setTryColour(3);
									
								} else if (counter == 4) {
									
									triesColourButton_2_1.setBackground(Color.YELLOW);
									checkFirstTryColour.setTryColour(4);
									
								} else if (counter == 5) {
									
									triesColourButton_2_1.setBackground(Color.MAGENTA);
									checkFirstTryColour.setTryColour(5);
									
								} else if (counter == 6) {
									
									triesColourButton_2_1.setBackground(Color.LIGHT_GRAY);
									checkFirstTryColour.setTryColour(6);
									counter = 0;
									
								}
								
							}
							
						});
						
						triesColourButton_2_2.addActionListener(new ActionListener() {
							
							int counter = 0;
							
							public void actionPerformed (ActionEvent e) {
								
								counter++;
								
								if (counter == 1) {
									
									triesColourButton_2_2.setBackground(Color.BLUE);
									checkSecondTryColour.setTryColour(1);
									
								} else if (counter == 2) {
									
									triesColourButton_2_2.setBackground(Color.RED);
									checkSecondTryColour.setTryColour(2);
									
								} else if (counter == 3) {
									
									triesColourButton_2_2.setBackground(Color.GREEN);
									checkSecondTryColour.setTryColour(3);
									
								} else if (counter == 4) {
									
									triesColourButton_2_2.setBackground(Color.YELLOW);
									checkSecondTryColour.setTryColour(4);
									
								} else if (counter == 5) {
									
									triesColourButton_2_2.setBackground(Color.MAGENTA);
									checkSecondTryColour.setTryColour(5);
									
								} else if (counter == 6) {
									
									triesColourButton_2_2.setBackground(Color.LIGHT_GRAY);
									checkSecondTryColour.setTryColour(6);
									counter = 0;
									
								}
								
							}
							
						});
						
						triesColourButton_2_3.addActionListener(new ActionListener() {
							
							int counter = 0;
							
							public void actionPerformed (ActionEvent e) {
								
								counter++;
								
								if (counter == 1) {
									
									triesColourButton_2_3.setBackground(Color.BLUE);
									checkThirdTryColour.setTryColour(1);
									
								} else if (counter == 2) {
									
									triesColourButton_2_3.setBackground(Color.RED);
									checkThirdTryColour.setTryColour(2);
									
								} else if (counter == 3) {
									
									triesColourButton_2_3.setBackground(Color.GREEN);
									checkThirdTryColour.setTryColour(3);
									
								} else if (counter == 4) {
									
									triesColourButton_2_3.setBackground(Color.YELLOW);
									checkThirdTryColour.setTryColour(4);
									
								} else if (counter == 5) {
									
									triesColourButton_2_3.setBackground(Color.MAGENTA);
									checkThirdTryColour.setTryColour(5);
									
								} else if (counter == 6) {
									
									triesColourButton_2_3.setBackground(Color.LIGHT_GRAY);
									checkThirdTryColour.setTryColour(6);
									counter = 0;
									
								}
								
							}
							
						});
						
						triesColourButton_2_4.addActionListener(new ActionListener() {
							
							int counter = 0;
							
							public void actionPerformed (ActionEvent e) {
								
								counter++;
								
								if (counter == 1) {
									
									triesColourButton_2_4.setBackground(Color.BLUE);
									checkFourthTryColour.setTryColour(1);
									
								} else if (counter == 2) {
									
									triesColourButton_2_4.setBackground(Color.RED);
									checkFourthTryColour.setTryColour(2);
									
								} else if (counter == 3) {
									
									triesColourButton_2_4.setBackground(Color.GREEN);
									checkFourthTryColour.setTryColour(3);
									
								} else if (counter == 4) {
									
									triesColourButton_2_4.setBackground(Color.YELLOW);
									checkFourthTryColour.setTryColour(4);
									
								} else if (counter == 5) {
									
									triesColourButton_2_4.setBackground(Color.MAGENTA);
									checkFourthTryColour.setTryColour(5);
									
								} else if (counter == 6) {
									
									triesColourButton_2_4.setBackground(Color.LIGHT_GRAY);
									checkFourthTryColour.setTryColour(6);
									counter = 0;
									
								}
								
							}
							
						});
						
					}
					
				});		
				
				checkButton_2.addActionListener(new ActionListener() {
					
					public void actionPerformed (ActionEvent e) {
						
						checkButton_2.setEnabled(false);
						checkButton_3.setEnabled(true);
						
						int countHits = 0;
						int countBlows = 0;
						
						if (checkFirstCombinationColour.getCombinationColour() == 1 && checkFirstTryColour.getTryColour() == 1) {
							
							hitsColourButton_2_1.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 1 && checkFirstTryColour.getTryColour() == 1) {
							
							hitsColourButton_2_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 1 && checkFirstTryColour.getTryColour() == 1) {
							
							hitsColourButton_2_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 1 && checkFirstTryColour.getTryColour() == 1) {
							
							hitsColourButton_2_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 2 && checkFirstTryColour.getTryColour() == 2) {
							
							hitsColourButton_2_1.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 2 && checkFirstTryColour.getTryColour() == 2) {
							
							hitsColourButton_2_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 2 && checkFirstTryColour.getTryColour() == 2) {
							
							hitsColourButton_2_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 2 && checkFirstTryColour.getTryColour() == 2) {
							
							hitsColourButton_2_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 3 && checkFirstTryColour.getTryColour() == 3) {
							
							hitsColourButton_2_1.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 3 && checkFirstTryColour.getTryColour() == 3) {
							
							hitsColourButton_2_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 3 && checkFirstTryColour.getTryColour() == 3) {
							
							hitsColourButton_2_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 3 && checkFirstTryColour.getTryColour() == 3) {
							
							hitsColourButton_2_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 4 && checkFirstTryColour.getTryColour() == 4) {
							
							hitsColourButton_2_1.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 4 && checkFirstTryColour.getTryColour() == 4) {
							
							hitsColourButton_2_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 4 && checkFirstTryColour.getTryColour() == 4) {
							
							hitsColourButton_2_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 4 && checkFirstTryColour.getTryColour() == 4) {
							
							hitsColourButton_2_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 5 && checkFirstTryColour.getTryColour() == 5) {
							
							hitsColourButton_2_1.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 5 && checkFirstTryColour.getTryColour() == 5) {
							
							hitsColourButton_2_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 5 && checkFirstTryColour.getTryColour() == 5) {
							
							hitsColourButton_2_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 5 && checkFirstTryColour.getTryColour() == 5) {
							
							hitsColourButton_2_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 6 && checkFirstTryColour.getTryColour() == 6) {
							
							hitsColourButton_2_1.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 6 && checkFirstTryColour.getTryColour() == 6) {
							
							hitsColourButton_2_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 6 && checkFirstTryColour.getTryColour() == 6) {
							
							hitsColourButton_2_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 6 && checkFirstTryColour.getTryColour() == 6) {
							
							hitsColourButton_2_1.setBackground(Color.WHITE);
							countBlows++;
							
						}
						
						if (checkSecondCombinationColour.getCombinationColour() == 1 && checkSecondTryColour.getTryColour() == 1) {
							
							hitsColourButton_2_2.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 1 && checkSecondTryColour.getTryColour() == 1) {
							
							hitsColourButton_2_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 1 && checkSecondTryColour.getTryColour() == 1) {
							
							hitsColourButton_2_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 1 && checkSecondTryColour.getTryColour() == 1) {
							
							hitsColourButton_2_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 2 && checkSecondTryColour.getTryColour() == 2) {
							
							hitsColourButton_2_2.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 2 && checkSecondTryColour.getTryColour() == 2) {
							
							hitsColourButton_2_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 2 && checkSecondTryColour.getTryColour() == 2) {
							
							hitsColourButton_2_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 2 && checkSecondTryColour.getTryColour() == 2) {
							
							hitsColourButton_2_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 3 && checkSecondTryColour.getTryColour() == 3) {
							
							hitsColourButton_2_2.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 3 && checkSecondTryColour.getTryColour() == 3) {
							
							hitsColourButton_2_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 3 && checkSecondTryColour.getTryColour() == 3) {
							
							hitsColourButton_2_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 3 && checkSecondTryColour.getTryColour() == 3) {
							
							hitsColourButton_2_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 4 && checkSecondTryColour.getTryColour() == 4) {
							
							hitsColourButton_2_2.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 4 && checkSecondTryColour.getTryColour() == 4) {
							
							hitsColourButton_2_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 4 && checkSecondTryColour.getTryColour() == 4) {
							
							hitsColourButton_2_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 4 && checkSecondTryColour.getTryColour() == 4) {
							
							hitsColourButton_2_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 5 && checkSecondTryColour.getTryColour() == 5) {
							
							hitsColourButton_2_2.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 5 && checkSecondTryColour.getTryColour() == 5) {
							
							hitsColourButton_2_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 5 && checkSecondTryColour.getTryColour() == 5) {
							
							hitsColourButton_2_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 5 && checkSecondTryColour.getTryColour() == 5) {
							
							hitsColourButton_2_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 6 && checkSecondTryColour.getTryColour() == 6) {
							
							hitsColourButton_2_2.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 6 && checkSecondTryColour.getTryColour() == 6) {
							
							hitsColourButton_2_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 6 && checkSecondTryColour.getTryColour() == 6) {
							
							hitsColourButton_2_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 6 && checkSecondTryColour.getTryColour() == 6) {
							
							hitsColourButton_2_2.setBackground(Color.WHITE);
							countBlows++;
							
						}
						
						if (checkThirdCombinationColour.getCombinationColour() == 1 && checkThirdTryColour.getTryColour() == 1) {
							
							hitsColourButton_2_3.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 1 && checkThirdTryColour.getTryColour() == 1) {
							
							hitsColourButton_2_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 1 && checkThirdTryColour.getTryColour() == 1) {
							
							hitsColourButton_2_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 1 && checkThirdTryColour.getTryColour() == 1) {
							
							hitsColourButton_2_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 2 && checkThirdTryColour.getTryColour() == 2) {
							
							hitsColourButton_2_3.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 2 && checkThirdTryColour.getTryColour() == 2) {
							
							hitsColourButton_2_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 2 && checkThirdTryColour.getTryColour() == 2) {
							
							hitsColourButton_2_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 2 && checkThirdTryColour.getTryColour() == 2) {
							
							hitsColourButton_2_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 3 && checkThirdTryColour.getTryColour() == 3) {
							
							hitsColourButton_2_3.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 3 && checkThirdTryColour.getTryColour() == 3) {
							
							hitsColourButton_2_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 3 && checkThirdTryColour.getTryColour() == 3) {
							
							hitsColourButton_2_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 3 && checkThirdTryColour.getTryColour() == 3) {
							
							hitsColourButton_2_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 4 && checkThirdTryColour.getTryColour() == 4) {
							
							hitsColourButton_2_3.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 4 && checkThirdTryColour.getTryColour() == 4) {
							
							hitsColourButton_2_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 4 && checkThirdTryColour.getTryColour() == 4) {
							
							hitsColourButton_2_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 4 && checkThirdTryColour.getTryColour() == 4) {
							
							hitsColourButton_2_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 5 && checkThirdTryColour.getTryColour() == 5) {
							
							hitsColourButton_2_3.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 5 && checkThirdTryColour.getTryColour() == 5) {
							
							hitsColourButton_2_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 5 && checkThirdTryColour.getTryColour() == 5) {
							
							hitsColourButton_2_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 5 && checkThirdTryColour.getTryColour() == 5) {
							
							hitsColourButton_2_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 6 && checkThirdTryColour.getTryColour() == 6) {
							
							hitsColourButton_2_3.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 6 && checkThirdTryColour.getTryColour() == 6) {
							
							hitsColourButton_2_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 6 && checkThirdTryColour.getTryColour() == 6) {
							
							hitsColourButton_2_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 6 && checkThirdTryColour.getTryColour() == 6) {
							
							hitsColourButton_2_3.setBackground(Color.WHITE);
							countBlows++;
							
						}
						
						if (checkFourthCombinationColour.getCombinationColour() == 1 && checkFourthTryColour.getTryColour() == 1) {
							
							hitsColourButton_2_4.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 1 && checkFourthTryColour.getTryColour() == 1) {
							
							hitsColourButton_2_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 1 && checkFourthTryColour.getTryColour() == 1) {
							
							hitsColourButton_2_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 1 && checkFourthTryColour.getTryColour() == 1) {
							
							hitsColourButton_2_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 2 && checkFourthTryColour.getTryColour() == 2) {
							
							hitsColourButton_2_4.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 2 && checkFourthTryColour.getTryColour() == 2) {
							
							hitsColourButton_2_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 2 && checkFourthTryColour.getTryColour() == 2) {
							
							hitsColourButton_2_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 2 && checkFourthTryColour.getTryColour() == 2) {
							
							hitsColourButton_2_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 3 && checkFourthTryColour.getTryColour() == 3) {
							
							hitsColourButton_2_4.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 3 && checkFourthTryColour.getTryColour() == 3) {
							
							hitsColourButton_2_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 3 && checkFourthTryColour.getTryColour() == 3) {
							
							hitsColourButton_2_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 3 && checkFourthTryColour.getTryColour() == 3) {
							
							hitsColourButton_2_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 4 && checkFourthTryColour.getTryColour() == 4) {
							
							hitsColourButton_2_4.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 4 && checkFourthTryColour.getTryColour() == 4) {
							
							hitsColourButton_2_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 4 && checkFourthTryColour.getTryColour() == 4) {
							
							hitsColourButton_2_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 4 && checkFourthTryColour.getTryColour() == 4) {
							
							hitsColourButton_2_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 5 && checkFourthTryColour.getTryColour() == 5) {
							
							hitsColourButton_2_4.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 5 && checkFourthTryColour.getTryColour() == 5) {
							
							hitsColourButton_2_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 5 && checkFourthTryColour.getTryColour() == 5) {
							
							hitsColourButton_2_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 5 && checkFourthTryColour.getTryColour() == 5) {
							
							hitsColourButton_2_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 6 && checkFourthTryColour.getTryColour() == 6) {
							
							hitsColourButton_2_4.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 6 && checkFourthTryColour.getTryColour() == 6) {
							
							hitsColourButton_2_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 6 && checkFourthTryColour.getTryColour() == 6) {
							
							hitsColourButton_2_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 6 && checkFourthTryColour.getTryColour() == 6) {
							
							hitsColourButton_2_4.setBackground(Color.WHITE);
							countBlows++;
							
						}
						
						if (countHits > 1 && countHits < 4 && countBlows > 1) {
							
							JOptionPane.showMessageDialog(null, countHits + " Hits! " + countBlows + " Blows!");
							
						} else if (countHits > 1 && countHits < 4 && countBlows == 1) {
							
							JOptionPane.showMessageDialog(null, countHits + " Hits! " + countBlows + " Blow!");
							
						} else if (countHits == 1 && countBlows > 1) {
							
							JOptionPane.showMessageDialog(null, countHits + " Hit! " + countBlows + " Blows!");
							
						} else if (countHits == 1 && countBlows == 1) {
							
							JOptionPane.showMessageDialog(null, countHits + " Hit! " + countBlows + " Blow!");
							
						} else if (countHits > 1 && countHits < 4 && countBlows < 1) {
							
							JOptionPane.showMessageDialog(null, countHits + " Hits! " + countBlows + " Blows!");
							
						} else if (countHits == 1 && countBlows < 1) {
							
							JOptionPane.showMessageDialog(null, countHits + " Hit! " + countBlows + " Blows!");
							
						} else if (countHits < 1 && countBlows > 1) {
							
							JOptionPane.showMessageDialog(null, countHits + " Hits! " + countBlows + " Blows!");
							
						} else if (countHits < 1 && countBlows == 1) {
							
							JOptionPane.showMessageDialog(null, countHits + " Hits! " + countBlows + " Blow!");
							
						} else if (countHits < 1 && countBlows < 1) {
							
							JOptionPane.showMessageDialog(null, countHits + " Hits! " + countBlows + " Blows!");
							
						} else if (countHits == 4) {
							
							checkButton_3.setEnabled(false);
							
							JOptionPane.showMessageDialog(null, "You won the game!");
							
						}
						
						triesColourButton_3_1.addActionListener(new ActionListener() {
							
							int counter = 0;
							
							public void actionPerformed (ActionEvent e) {
								
								counter++;
								
								if (counter == 1) {
									
									triesColourButton_3_1.setBackground(Color.BLUE);
									checkFirstTryColour.setTryColour(1);
									
								} else if (counter == 2) {
									
									triesColourButton_3_1.setBackground(Color.RED);
									checkFirstTryColour.setTryColour(2);
									
								} else if (counter == 3) {
									
									triesColourButton_3_1.setBackground(Color.GREEN);
									checkFirstTryColour.setTryColour(3);
									
								} else if (counter == 4) {
									
									triesColourButton_3_1.setBackground(Color.YELLOW);
									checkFirstTryColour.setTryColour(4);
									
								} else if (counter == 5) {
									
									triesColourButton_3_1.setBackground(Color.MAGENTA);
									checkFirstTryColour.setTryColour(5);
									
								} else if (counter == 6) {
									
									triesColourButton_3_1.setBackground(Color.LIGHT_GRAY);
									checkFirstTryColour.setTryColour(6);
									counter = 0;
									
								}
								
							}
							
						});
						
						triesColourButton_3_2.addActionListener(new ActionListener() {
							
							int counter = 0;
							
							public void actionPerformed (ActionEvent e) {
								
								counter++;
								
								if (counter == 1) {
									
									triesColourButton_3_2.setBackground(Color.BLUE);
									checkSecondTryColour.setTryColour(1);
									
								} else if (counter == 2) {
									
									triesColourButton_3_2.setBackground(Color.RED);
									checkSecondTryColour.setTryColour(2);
									
								} else if (counter == 3) {
									
									triesColourButton_3_2.setBackground(Color.GREEN);
									checkSecondTryColour.setTryColour(3);
									
								} else if (counter == 4) {
									
									triesColourButton_3_2.setBackground(Color.YELLOW);
									checkSecondTryColour.setTryColour(4);
									
								} else if (counter == 5) {
									
									triesColourButton_3_2.setBackground(Color.MAGENTA);
									checkSecondTryColour.setTryColour(5);
									
								} else if (counter == 6) {
									
									triesColourButton_3_2.setBackground(Color.LIGHT_GRAY);
									checkSecondTryColour.setTryColour(6);
									counter = 0;
									
								}
								
							}
							
						});
						
						triesColourButton_3_3.addActionListener(new ActionListener() {
							
							int counter = 0;
							
							public void actionPerformed (ActionEvent e) {
								
								counter++;
								
								if (counter == 1) {
									
									triesColourButton_3_3.setBackground(Color.BLUE);
									checkThirdTryColour.setTryColour(1);
									
								} else if (counter == 2) {
									
									triesColourButton_3_3.setBackground(Color.RED);
									checkThirdTryColour.setTryColour(2);
									
								} else if (counter == 3) {
									
									triesColourButton_3_3.setBackground(Color.GREEN);
									checkThirdTryColour.setTryColour(3);
									
								} else if (counter == 4) {
									
									triesColourButton_3_3.setBackground(Color.YELLOW);
									checkThirdTryColour.setTryColour(4);
									
								} else if (counter == 5) {
									
									triesColourButton_3_3.setBackground(Color.MAGENTA);
									checkThirdTryColour.setTryColour(5);
									
								} else if (counter == 6) {
									
									triesColourButton_3_3.setBackground(Color.LIGHT_GRAY);
									checkThirdTryColour.setTryColour(6);
									counter = 0;
									
								}
								
							}
							
						});
						
						triesColourButton_3_4.addActionListener(new ActionListener() {
							
							int counter = 0;
							
							public void actionPerformed (ActionEvent e) {
								
								counter++;
								
								if (counter == 1) {
									
									triesColourButton_3_4.setBackground(Color.BLUE);
									checkFourthTryColour.setTryColour(1);
									
								} else if (counter == 2) {
									
									triesColourButton_3_4.setBackground(Color.RED);
									checkFourthTryColour.setTryColour(2);
									
								} else if (counter == 3) {
									
									triesColourButton_3_4.setBackground(Color.GREEN);
									checkFourthTryColour.setTryColour(3);
									
								} else if (counter == 4) {
									
									triesColourButton_3_4.setBackground(Color.YELLOW);
									checkFourthTryColour.setTryColour(4);
									
								} else if (counter == 5) {
									
									triesColourButton_3_4.setBackground(Color.MAGENTA);
									checkFourthTryColour.setTryColour(5);
									
								} else if (counter == 6) {
									
									triesColourButton_3_4.setBackground(Color.LIGHT_GRAY);
									checkFourthTryColour.setTryColour(6);
									counter = 0;
									
								}
								
							}
							
						});
						
					}
					
				});				
				
				checkButton_3.addActionListener(new ActionListener() {
					
					public void actionPerformed (ActionEvent e) {
						
						checkButton_3.setEnabled(false);
						checkButton_4.setEnabled(true);
						
						int countHits = 0;
						int countBlows = 0;
						
						if (checkFirstCombinationColour.getCombinationColour() == 1 && checkFirstTryColour.getTryColour() == 1) {
							
							hitsColourButton_3_1.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 1 && checkFirstTryColour.getTryColour() == 1) {
							
							hitsColourButton_3_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 1 && checkFirstTryColour.getTryColour() == 1) {
							
							hitsColourButton_3_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 1 && checkFirstTryColour.getTryColour() == 1) {
							
							hitsColourButton_3_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 2 && checkFirstTryColour.getTryColour() == 2) {
							
							hitsColourButton_3_1.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 2 && checkFirstTryColour.getTryColour() == 2) {
							
							hitsColourButton_3_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 2 && checkFirstTryColour.getTryColour() == 2) {
							
							hitsColourButton_3_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 2 && checkFirstTryColour.getTryColour() == 2) {
							
							hitsColourButton_3_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 3 && checkFirstTryColour.getTryColour() == 3) {
							
							hitsColourButton_3_1.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 3 && checkFirstTryColour.getTryColour() == 3) {
							
							hitsColourButton_3_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 3 && checkFirstTryColour.getTryColour() == 3) {
							
							hitsColourButton_3_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 3 && checkFirstTryColour.getTryColour() == 3) {
							
							hitsColourButton_3_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 4 && checkFirstTryColour.getTryColour() == 4) {
							
							hitsColourButton_3_1.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 4 && checkFirstTryColour.getTryColour() == 4) {
							
							hitsColourButton_3_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 4 && checkFirstTryColour.getTryColour() == 4) {
							
							hitsColourButton_3_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 4 && checkFirstTryColour.getTryColour() == 4) {
							
							hitsColourButton_3_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 5 && checkFirstTryColour.getTryColour() == 5) {
							
							hitsColourButton_3_1.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 5 && checkFirstTryColour.getTryColour() == 5) {
							
							hitsColourButton_3_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 5 && checkFirstTryColour.getTryColour() == 5) {
							
							hitsColourButton_3_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 5 && checkFirstTryColour.getTryColour() == 5) {
							
							hitsColourButton_3_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 6 && checkFirstTryColour.getTryColour() == 6) {
							
							hitsColourButton_3_1.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 6 && checkFirstTryColour.getTryColour() == 6) {
							
							hitsColourButton_3_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 6 && checkFirstTryColour.getTryColour() == 6) {
							
							hitsColourButton_3_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 6 && checkFirstTryColour.getTryColour() == 6) {
							
							hitsColourButton_3_1.setBackground(Color.WHITE);
							countBlows++;
							
						}
						
						if (checkSecondCombinationColour.getCombinationColour() == 1 && checkSecondTryColour.getTryColour() == 1) {
							
							hitsColourButton_3_2.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 1 && checkSecondTryColour.getTryColour() == 1) {
							
							hitsColourButton_3_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 1 && checkSecondTryColour.getTryColour() == 1) {
							
							hitsColourButton_3_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 1 && checkSecondTryColour.getTryColour() == 1) {
							
							hitsColourButton_3_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 2 && checkSecondTryColour.getTryColour() == 2) {
							
							hitsColourButton_3_2.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 2 && checkSecondTryColour.getTryColour() == 2) {
							
							hitsColourButton_3_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 2 && checkSecondTryColour.getTryColour() == 2) {
							
							hitsColourButton_3_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 2 && checkSecondTryColour.getTryColour() == 2) {
							
							hitsColourButton_3_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 3 && checkSecondTryColour.getTryColour() == 3) {
							
							hitsColourButton_3_2.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 3 && checkSecondTryColour.getTryColour() == 3) {
							
							hitsColourButton_3_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 3 && checkSecondTryColour.getTryColour() == 3) {
							
							hitsColourButton_3_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 3 && checkSecondTryColour.getTryColour() == 3) {
							
							hitsColourButton_3_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 4 && checkSecondTryColour.getTryColour() == 4) {
							
							hitsColourButton_3_2.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 4 && checkSecondTryColour.getTryColour() == 4) {
							
							hitsColourButton_3_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 4 && checkSecondTryColour.getTryColour() == 4) {
							
							hitsColourButton_3_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 4 && checkSecondTryColour.getTryColour() == 4) {
							
							hitsColourButton_3_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 5 && checkSecondTryColour.getTryColour() == 5) {
							
							hitsColourButton_3_2.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 5 && checkSecondTryColour.getTryColour() == 5) {
							
							hitsColourButton_3_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 5 && checkSecondTryColour.getTryColour() == 5) {
							
							hitsColourButton_3_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 5 && checkSecondTryColour.getTryColour() == 5) {
							
							hitsColourButton_3_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 6 && checkSecondTryColour.getTryColour() == 6) {
							
							hitsColourButton_3_2.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 6 && checkSecondTryColour.getTryColour() == 6) {
							
							hitsColourButton_3_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 6 && checkSecondTryColour.getTryColour() == 6) {
							
							hitsColourButton_3_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 6 && checkSecondTryColour.getTryColour() == 6) {
							
							hitsColourButton_3_2.setBackground(Color.WHITE);
							countBlows++;
							
						}
						
						if (checkThirdCombinationColour.getCombinationColour() == 1 && checkThirdTryColour.getTryColour() == 1) {
							
							hitsColourButton_3_3.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 1 && checkThirdTryColour.getTryColour() == 1) {
							
							hitsColourButton_3_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 1 && checkThirdTryColour.getTryColour() == 1) {
							
							hitsColourButton_3_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 1 && checkThirdTryColour.getTryColour() == 1) {
							
							hitsColourButton_3_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 2 && checkThirdTryColour.getTryColour() == 2) {
							
							hitsColourButton_3_3.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 2 && checkThirdTryColour.getTryColour() == 2) {
							
							hitsColourButton_3_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 2 && checkThirdTryColour.getTryColour() == 2) {
							
							hitsColourButton_3_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 2 && checkThirdTryColour.getTryColour() == 2) {
							
							hitsColourButton_3_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 3 && checkThirdTryColour.getTryColour() == 3) {
							
							hitsColourButton_3_3.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 3 && checkThirdTryColour.getTryColour() == 3) {
							
							hitsColourButton_3_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 3 && checkThirdTryColour.getTryColour() == 3) {
							
							hitsColourButton_3_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 3 && checkThirdTryColour.getTryColour() == 3) {
							
							hitsColourButton_3_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 4 && checkThirdTryColour.getTryColour() == 4) {
							
							hitsColourButton_3_3.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 4 && checkThirdTryColour.getTryColour() == 4) {
							
							hitsColourButton_3_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 4 && checkThirdTryColour.getTryColour() == 4) {
							
							hitsColourButton_3_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 4 && checkThirdTryColour.getTryColour() == 4) {
							
							hitsColourButton_3_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 5 && checkThirdTryColour.getTryColour() == 5) {
							
							hitsColourButton_3_3.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 5 && checkThirdTryColour.getTryColour() == 5) {
							
							hitsColourButton_3_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 5 && checkThirdTryColour.getTryColour() == 5) {
							
							hitsColourButton_3_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 5 && checkThirdTryColour.getTryColour() == 5) {
							
							hitsColourButton_3_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 6 && checkThirdTryColour.getTryColour() == 6) {
							
							hitsColourButton_3_3.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 6 && checkThirdTryColour.getTryColour() == 6) {
							
							hitsColourButton_3_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 6 && checkThirdTryColour.getTryColour() == 6) {
							
							hitsColourButton_3_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 6 && checkThirdTryColour.getTryColour() == 6) {
							
							hitsColourButton_3_3.setBackground(Color.WHITE);
							countBlows++;
							
						}
						
						if (checkFourthCombinationColour.getCombinationColour() == 1 && checkFourthTryColour.getTryColour() == 1) {
							
							hitsColourButton_3_4.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 1 && checkFourthTryColour.getTryColour() == 1) {
							
							hitsColourButton_3_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 1 && checkFourthTryColour.getTryColour() == 1) {
							
							hitsColourButton_3_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 1 && checkFourthTryColour.getTryColour() == 1) {
							
							hitsColourButton_3_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 2 && checkFourthTryColour.getTryColour() == 2) {
							
							hitsColourButton_3_4.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 2 && checkFourthTryColour.getTryColour() == 2) {
							
							hitsColourButton_3_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 2 && checkFourthTryColour.getTryColour() == 2) {
							
							hitsColourButton_3_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 2 && checkFourthTryColour.getTryColour() == 2) {
							
							hitsColourButton_3_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 3 && checkFourthTryColour.getTryColour() == 3) {
							
							hitsColourButton_3_4.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 3 && checkFourthTryColour.getTryColour() == 3) {
							
							hitsColourButton_3_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 3 && checkFourthTryColour.getTryColour() == 3) {
							
							hitsColourButton_3_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 3 && checkFourthTryColour.getTryColour() == 3) {
							
							hitsColourButton_3_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 4 && checkFourthTryColour.getTryColour() == 4) {
							
							hitsColourButton_3_4.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 4 && checkFourthTryColour.getTryColour() == 4) {
							
							hitsColourButton_3_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 4 && checkFourthTryColour.getTryColour() == 4) {
							
							hitsColourButton_3_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 4 && checkFourthTryColour.getTryColour() == 4) {
							
							hitsColourButton_3_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 5 && checkFourthTryColour.getTryColour() == 5) {
							
							hitsColourButton_3_4.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 5 && checkFourthTryColour.getTryColour() == 5) {
							
							hitsColourButton_3_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 5 && checkFourthTryColour.getTryColour() == 5) {
							
							hitsColourButton_3_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 5 && checkFourthTryColour.getTryColour() == 5) {
							
							hitsColourButton_3_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 6 && checkFourthTryColour.getTryColour() == 6) {
							
							hitsColourButton_3_4.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 6 && checkFourthTryColour.getTryColour() == 6) {
							
							hitsColourButton_3_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 6 && checkFourthTryColour.getTryColour() == 6) {
							
							hitsColourButton_3_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 6 && checkFourthTryColour.getTryColour() == 6) {
							
							hitsColourButton_3_4.setBackground(Color.WHITE);
							countBlows++;
							
						}
						
						if (countHits > 1 && countHits < 4 && countBlows > 1) {
							
							JOptionPane.showMessageDialog(null, countHits + " Hits! " + countBlows + " Blows!");
							
						} else if (countHits > 1 && countHits < 4 && countBlows == 1) {
							
							JOptionPane.showMessageDialog(null, countHits + " Hits! " + countBlows + " Blow!");
							
						} else if (countHits == 1 && countBlows > 1) {
							
							JOptionPane.showMessageDialog(null, countHits + " Hit! " + countBlows + " Blows!");
							
						} else if (countHits == 1 && countBlows == 1) {
							
							JOptionPane.showMessageDialog(null, countHits + " Hit! " + countBlows + " Blow!");
							
						} else if (countHits > 1 && countHits < 4 && countBlows < 1) {
							
							JOptionPane.showMessageDialog(null, countHits + " Hits! " + countBlows + " Blows!");
							
						} else if (countHits == 1 && countBlows < 1) {
							
							JOptionPane.showMessageDialog(null, countHits + " Hit! " + countBlows + " Blows!");
							
						} else if (countHits < 1 && countBlows > 1) {
							
							JOptionPane.showMessageDialog(null, countHits + " Hits! " + countBlows + " Blows!");
							
						} else if (countHits < 1 && countBlows == 1) {
							
							JOptionPane.showMessageDialog(null, countHits + " Hits! " + countBlows + " Blow!");
							
						} else if (countHits < 1 && countBlows < 1) {
							
							JOptionPane.showMessageDialog(null, countHits + " Hits! " + countBlows + " Blows!");
							
						} else if (countHits == 4) {
							
							checkButton_4.setEnabled(false);
							
							JOptionPane.showMessageDialog(null, "You won the game!");
							
						}
						
						triesColourButton_4_1.addActionListener(new ActionListener() {
							
							int counter = 0;
							
							public void actionPerformed (ActionEvent e) {
								
								counter++;
								
								if (counter == 1) {
									
									triesColourButton_4_1.setBackground(Color.BLUE);
									checkFirstTryColour.setTryColour(1);
									
								} else if (counter == 2) {
									
									triesColourButton_4_1.setBackground(Color.RED);
									checkFirstTryColour.setTryColour(2);
									
								} else if (counter == 3) {
									
									triesColourButton_4_1.setBackground(Color.GREEN);
									checkFirstTryColour.setTryColour(3);
									
								} else if (counter == 4) {
									
									triesColourButton_4_1.setBackground(Color.YELLOW);
									checkFirstTryColour.setTryColour(4);
									
								} else if (counter == 5) {
									
									triesColourButton_4_1.setBackground(Color.MAGENTA);
									checkFirstTryColour.setTryColour(5);
									
								} else if (counter == 6) {
									
									triesColourButton_4_1.setBackground(Color.LIGHT_GRAY);
									checkFirstTryColour.setTryColour(6);
									counter = 0;
									
								}
								
							}
							
						});
						
						triesColourButton_4_2.addActionListener(new ActionListener() {
							
							int counter = 0;
							
							public void actionPerformed (ActionEvent e) {
								
								counter++;
								
								if (counter == 1) {
									
									triesColourButton_4_2.setBackground(Color.BLUE);
									checkSecondTryColour.setTryColour(1);
									
								} else if (counter == 2) {
									
									triesColourButton_4_2.setBackground(Color.RED);
									checkSecondTryColour.setTryColour(2);
									
								} else if (counter == 3) {
									
									triesColourButton_4_2.setBackground(Color.GREEN);
									checkSecondTryColour.setTryColour(3);
									
								} else if (counter == 4) {
									
									triesColourButton_4_2.setBackground(Color.YELLOW);
									checkSecondTryColour.setTryColour(4);
									
								} else if (counter == 5) {
									
									triesColourButton_4_2.setBackground(Color.MAGENTA);
									checkSecondTryColour.setTryColour(5);
									
								} else if (counter == 6) {
									
									triesColourButton_4_2.setBackground(Color.LIGHT_GRAY);
									checkSecondTryColour.setTryColour(6);
									counter = 0;
									
								}
								
							}
							
						});
						
						triesColourButton_4_3.addActionListener(new ActionListener() {
							
							int counter = 0;
							
							public void actionPerformed (ActionEvent e) {
								
								counter++;
								
								if (counter == 1) {
									
									triesColourButton_4_3.setBackground(Color.BLUE);
									checkThirdTryColour.setTryColour(1);
									
								} else if (counter == 2) {
									
									triesColourButton_4_3.setBackground(Color.RED);
									checkThirdTryColour.setTryColour(2);
									
								} else if (counter == 3) {
									
									triesColourButton_4_3.setBackground(Color.GREEN);
									checkThirdTryColour.setTryColour(3);
									
								} else if (counter == 4) {
									
									triesColourButton_4_3.setBackground(Color.YELLOW);
									checkThirdTryColour.setTryColour(4);
									
								} else if (counter == 5) {
									
									triesColourButton_4_3.setBackground(Color.MAGENTA);
									checkThirdTryColour.setTryColour(5);
									
								} else if (counter == 6) {
									
									triesColourButton_4_3.setBackground(Color.LIGHT_GRAY);
									checkThirdTryColour.setTryColour(6);
									counter = 0;
									
								}
								
							}
							
						});
						
						triesColourButton_4_4.addActionListener(new ActionListener() {
							
							int counter = 0;
							
							public void actionPerformed (ActionEvent e) {
								
								counter++;
								
								if (counter == 1) {
									
									triesColourButton_4_4.setBackground(Color.BLUE);
									checkFourthTryColour.setTryColour(1);
									
								} else if (counter == 2) {
									
									triesColourButton_4_4.setBackground(Color.RED);
									checkFourthTryColour.setTryColour(2);
									
								} else if (counter == 3) {
									
									triesColourButton_4_4.setBackground(Color.GREEN);
									checkFourthTryColour.setTryColour(3);
									
								} else if (counter == 4) {
									
									triesColourButton_4_4.setBackground(Color.YELLOW);
									checkFourthTryColour.setTryColour(4);
									
								} else if (counter == 5) {
									
									triesColourButton_4_4.setBackground(Color.MAGENTA);
									checkFourthTryColour.setTryColour(5);
									
								} else if (counter == 6) {
									
									triesColourButton_4_4.setBackground(Color.LIGHT_GRAY);
									checkFourthTryColour.setTryColour(6);
									counter = 0;
									
								}
								
							}
							
						});
						
					}
					
				});
				
				checkButton_4.addActionListener(new ActionListener() {
					
					public void actionPerformed (ActionEvent e) {
						
						checkButton_4.setEnabled(false);
						checkButton_5.setEnabled(true);
						
						int countHits = 0;
						int countBlows = 0;
						
						if (checkFirstCombinationColour.getCombinationColour() == 1 && checkFirstTryColour.getTryColour() == 1) {
							
							hitsColourButton_4_1.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 1 && checkFirstTryColour.getTryColour() == 1) {
							
							hitsColourButton_4_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 1 && checkFirstTryColour.getTryColour() == 1) {
							
							hitsColourButton_4_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 1 && checkFirstTryColour.getTryColour() == 1) {
							
							hitsColourButton_4_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 2 && checkFirstTryColour.getTryColour() == 2) {
							
							hitsColourButton_4_1.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 2 && checkFirstTryColour.getTryColour() == 2) {
							
							hitsColourButton_4_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 2 && checkFirstTryColour.getTryColour() == 2) {
							
							hitsColourButton_4_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 2 && checkFirstTryColour.getTryColour() == 2) {
							
							hitsColourButton_4_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 3 && checkFirstTryColour.getTryColour() == 3) {
							
							hitsColourButton_4_1.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 3 && checkFirstTryColour.getTryColour() == 3) {
							
							hitsColourButton_4_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 3 && checkFirstTryColour.getTryColour() == 3) {
							
							hitsColourButton_4_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 3 && checkFirstTryColour.getTryColour() == 3) {
							
							hitsColourButton_4_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 4 && checkFirstTryColour.getTryColour() == 4) {
							
							hitsColourButton_4_1.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 4 && checkFirstTryColour.getTryColour() == 4) {
							
							hitsColourButton_4_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 4 && checkFirstTryColour.getTryColour() == 4) {
							
							hitsColourButton_4_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 4 && checkFirstTryColour.getTryColour() == 4) {
							
							hitsColourButton_4_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 5 && checkFirstTryColour.getTryColour() == 5) {
							
							hitsColourButton_4_1.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 5 && checkFirstTryColour.getTryColour() == 5) {
							
							hitsColourButton_4_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 5 && checkFirstTryColour.getTryColour() == 5) {
							
							hitsColourButton_4_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 5 && checkFirstTryColour.getTryColour() == 5) {
							
							hitsColourButton_4_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 6 && checkFirstTryColour.getTryColour() == 6) {
							
							hitsColourButton_4_1.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 6 && checkFirstTryColour.getTryColour() == 6) {
							
							hitsColourButton_4_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 6 && checkFirstTryColour.getTryColour() == 6) {
							
							hitsColourButton_4_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 6 && checkFirstTryColour.getTryColour() == 6) {
							
							hitsColourButton_4_1.setBackground(Color.WHITE);
							countBlows++;
							
						}
						
						if (checkSecondCombinationColour.getCombinationColour() == 1 && checkSecondTryColour.getTryColour() == 1) {
							
							hitsColourButton_4_2.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 1 && checkSecondTryColour.getTryColour() == 1) {
							
							hitsColourButton_4_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 1 && checkSecondTryColour.getTryColour() == 1) {
							
							hitsColourButton_4_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 1 && checkSecondTryColour.getTryColour() == 1) {
							
							hitsColourButton_4_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 2 && checkSecondTryColour.getTryColour() == 2) {
							
							hitsColourButton_4_2.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 2 && checkSecondTryColour.getTryColour() == 2) {
							
							hitsColourButton_4_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 2 && checkSecondTryColour.getTryColour() == 2) {
							
							hitsColourButton_4_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 2 && checkSecondTryColour.getTryColour() == 2) {
							
							hitsColourButton_4_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 3 && checkSecondTryColour.getTryColour() == 3) {
							
							hitsColourButton_4_2.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 3 && checkSecondTryColour.getTryColour() == 3) {
							
							hitsColourButton_4_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 3 && checkSecondTryColour.getTryColour() == 3) {
							
							hitsColourButton_4_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 3 && checkSecondTryColour.getTryColour() == 3) {
							
							hitsColourButton_4_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 4 && checkSecondTryColour.getTryColour() == 4) {
							
							hitsColourButton_4_2.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 4 && checkSecondTryColour.getTryColour() == 4) {
							
							hitsColourButton_4_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 4 && checkSecondTryColour.getTryColour() == 4) {
							
							hitsColourButton_4_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 4 && checkSecondTryColour.getTryColour() == 4) {
							
							hitsColourButton_4_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 5 && checkSecondTryColour.getTryColour() == 5) {
							
							hitsColourButton_4_2.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 5 && checkSecondTryColour.getTryColour() == 5) {
							
							hitsColourButton_4_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 5 && checkSecondTryColour.getTryColour() == 5) {
							
							hitsColourButton_4_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 5 && checkSecondTryColour.getTryColour() == 5) {
							
							hitsColourButton_4_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 6 && checkSecondTryColour.getTryColour() == 6) {
							
							hitsColourButton_4_2.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 6 && checkSecondTryColour.getTryColour() == 6) {
							
							hitsColourButton_4_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 6 && checkSecondTryColour.getTryColour() == 6) {
							
							hitsColourButton_4_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 6 && checkSecondTryColour.getTryColour() == 6) {
							
							hitsColourButton_4_2.setBackground(Color.WHITE);
							countBlows++;
							
						}
						
						if (checkThirdCombinationColour.getCombinationColour() == 1 && checkThirdTryColour.getTryColour() == 1) {
							
							hitsColourButton_4_3.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 1 && checkThirdTryColour.getTryColour() == 1) {
							
							hitsColourButton_4_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 1 && checkThirdTryColour.getTryColour() == 1) {
							
							hitsColourButton_4_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 1 && checkThirdTryColour.getTryColour() == 1) {
							
							hitsColourButton_4_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 2 && checkThirdTryColour.getTryColour() == 2) {
							
							hitsColourButton_4_3.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 2 && checkThirdTryColour.getTryColour() == 2) {
							
							hitsColourButton_4_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 2 && checkThirdTryColour.getTryColour() == 2) {
							
							hitsColourButton_4_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 2 && checkThirdTryColour.getTryColour() == 2) {
							
							hitsColourButton_4_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 3 && checkThirdTryColour.getTryColour() == 3) {
							
							hitsColourButton_4_3.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 3 && checkThirdTryColour.getTryColour() == 3) {
							
							hitsColourButton_4_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 3 && checkThirdTryColour.getTryColour() == 3) {
							
							hitsColourButton_4_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 3 && checkThirdTryColour.getTryColour() == 3) {
							
							hitsColourButton_4_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 4 && checkThirdTryColour.getTryColour() == 4) {
							
							hitsColourButton_4_3.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 4 && checkThirdTryColour.getTryColour() == 4) {
							
							hitsColourButton_4_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 4 && checkThirdTryColour.getTryColour() == 4) {
							
							hitsColourButton_4_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 4 && checkThirdTryColour.getTryColour() == 4) {
							
							hitsColourButton_4_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 5 && checkThirdTryColour.getTryColour() == 5) {
							
							hitsColourButton_4_3.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 5 && checkThirdTryColour.getTryColour() == 5) {
							
							hitsColourButton_4_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 5 && checkThirdTryColour.getTryColour() == 5) {
							
							hitsColourButton_4_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 5 && checkThirdTryColour.getTryColour() == 5) {
							
							hitsColourButton_4_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 6 && checkThirdTryColour.getTryColour() == 6) {
							
							hitsColourButton_4_3.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 6 && checkThirdTryColour.getTryColour() == 6) {
							
							hitsColourButton_4_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 6 && checkThirdTryColour.getTryColour() == 6) {
							
							hitsColourButton_4_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 6 && checkThirdTryColour.getTryColour() == 6) {
							
							hitsColourButton_4_3.setBackground(Color.WHITE);
							countBlows++;
							
						}
						
						if (checkFourthCombinationColour.getCombinationColour() == 1 && checkFourthTryColour.getTryColour() == 1) {
							
							hitsColourButton_4_4.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 1 && checkFourthTryColour.getTryColour() == 1) {
							
							hitsColourButton_4_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 1 && checkFourthTryColour.getTryColour() == 1) {
							
							hitsColourButton_4_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 1 && checkFourthTryColour.getTryColour() == 1) {
							
							hitsColourButton_4_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 2 && checkFourthTryColour.getTryColour() == 2) {
							
							hitsColourButton_4_4.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 2 && checkFourthTryColour.getTryColour() == 2) {
							
							hitsColourButton_4_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 2 && checkFourthTryColour.getTryColour() == 2) {
							
							hitsColourButton_4_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 2 && checkFourthTryColour.getTryColour() == 2) {
							
							hitsColourButton_4_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 3 && checkFourthTryColour.getTryColour() == 3) {
							
							hitsColourButton_4_4.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 3 && checkFourthTryColour.getTryColour() == 3) {
							
							hitsColourButton_4_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 3 && checkFourthTryColour.getTryColour() == 3) {
							
							hitsColourButton_4_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 3 && checkFourthTryColour.getTryColour() == 3) {
							
							hitsColourButton_4_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 4 && checkFourthTryColour.getTryColour() == 4) {
							
							hitsColourButton_4_4.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 4 && checkFourthTryColour.getTryColour() == 4) {
							
							hitsColourButton_4_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 4 && checkFourthTryColour.getTryColour() == 4) {
							
							hitsColourButton_4_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 4 && checkFourthTryColour.getTryColour() == 4) {
							
							hitsColourButton_4_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 5 && checkFourthTryColour.getTryColour() == 5) {
							
							hitsColourButton_4_4.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 5 && checkFourthTryColour.getTryColour() == 5) {
							
							hitsColourButton_4_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 5 && checkFourthTryColour.getTryColour() == 5) {
							
							hitsColourButton_4_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 5 && checkFourthTryColour.getTryColour() == 5) {
							
							hitsColourButton_4_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 6 && checkFourthTryColour.getTryColour() == 6) {
							
							hitsColourButton_4_4.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 6 && checkFourthTryColour.getTryColour() == 6) {
							
							hitsColourButton_4_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 6 && checkFourthTryColour.getTryColour() == 6) {
							
							hitsColourButton_4_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 6 && checkFourthTryColour.getTryColour() == 6) {
							
							hitsColourButton_4_4.setBackground(Color.WHITE);
							countBlows++;
							
						}
						
						if (countHits > 1 && countHits < 4 && countBlows > 1) {
							
							JOptionPane.showMessageDialog(null, countHits + " Hits! " + countBlows + " Blows!");
							
						} else if (countHits > 1 && countHits < 4 && countBlows == 1) {
							
							JOptionPane.showMessageDialog(null, countHits + " Hits! " + countBlows + " Blow!");
							
						} else if (countHits == 1 && countBlows > 1) {
							
							JOptionPane.showMessageDialog(null, countHits + " Hit! " + countBlows + " Blows!");
							
						} else if (countHits == 1 && countBlows == 1) {
							
							JOptionPane.showMessageDialog(null, countHits + " Hit! " + countBlows + " Blow!");
							
						} else if (countHits > 1 && countHits < 4 && countBlows < 1) {
							
							JOptionPane.showMessageDialog(null, countHits + " Hits! " + countBlows + " Blows!");
							
						} else if (countHits == 1 && countBlows < 1) {
							
							JOptionPane.showMessageDialog(null, countHits + " Hit! " + countBlows + " Blows!");
							
						} else if (countHits < 1 && countBlows > 1) {
							
							JOptionPane.showMessageDialog(null, countHits + " Hits! " + countBlows + " Blows!");
							
						} else if (countHits < 1 && countBlows == 1) {
							
							JOptionPane.showMessageDialog(null, countHits + " Hits! " + countBlows + " Blow!");
							
						} else if (countHits < 1 && countBlows < 1) {
							
							JOptionPane.showMessageDialog(null, countHits + " Hits! " + countBlows + " Blows!");
							
						} else if (countHits == 4) {
							
							checkButton_5.setEnabled(false);
							
							JOptionPane.showMessageDialog(null, "You won the game!");
							
						}
						
						triesColourButton_5_1.addActionListener(new ActionListener() {
							
							int counter = 0;
							
							public void actionPerformed (ActionEvent e) {
								
								counter++;
								
								if (counter == 1) {
									
									triesColourButton_5_1.setBackground(Color.BLUE);
									checkFirstTryColour.setTryColour(1);
									
								} else if (counter == 2) {
									
									triesColourButton_5_1.setBackground(Color.RED);
									checkFirstTryColour.setTryColour(2);
									
								} else if (counter == 3) {
									
									triesColourButton_5_1.setBackground(Color.GREEN);
									checkFirstTryColour.setTryColour(3);
									
								} else if (counter == 4) {
									
									triesColourButton_5_1.setBackground(Color.YELLOW);
									checkFirstTryColour.setTryColour(4);
									
								} else if (counter == 5) {
									
									triesColourButton_5_1.setBackground(Color.MAGENTA);
									checkFirstTryColour.setTryColour(5);
									
								} else if (counter == 6) {
									
									triesColourButton_5_1.setBackground(Color.LIGHT_GRAY);
									checkFirstTryColour.setTryColour(6);
									counter = 0;
									
								}
								
							}
							
						});
						
						triesColourButton_5_2.addActionListener(new ActionListener() {
							
							int counter = 0;
							
							public void actionPerformed (ActionEvent e) {
								
								counter++;
								
								if (counter == 1) {
									
									triesColourButton_5_2.setBackground(Color.BLUE);
									checkSecondTryColour.setTryColour(1);
									
								} else if (counter == 2) {
									
									triesColourButton_5_2.setBackground(Color.RED);
									checkSecondTryColour.setTryColour(2);
									
								} else if (counter == 3) {
									
									triesColourButton_5_2.setBackground(Color.GREEN);
									checkSecondTryColour.setTryColour(3);
									
								} else if (counter == 4) {
									
									triesColourButton_5_2.setBackground(Color.YELLOW);
									checkSecondTryColour.setTryColour(4);
									
								} else if (counter == 5) {
									
									triesColourButton_5_2.setBackground(Color.MAGENTA);
									checkSecondTryColour.setTryColour(5);
									
								} else if (counter == 6) {
									
									triesColourButton_5_2.setBackground(Color.LIGHT_GRAY);
									checkSecondTryColour.setTryColour(6);
									counter = 0;
									
								}
								
							}
							
						});
						
						triesColourButton_5_3.addActionListener(new ActionListener() {
							
							int counter = 0;
							
							public void actionPerformed (ActionEvent e) {
								
								counter++;
								
								if (counter == 1) {
									
									triesColourButton_5_3.setBackground(Color.BLUE);
									checkThirdTryColour.setTryColour(1);
									
								} else if (counter == 2) {
									
									triesColourButton_5_3.setBackground(Color.RED);
									checkThirdTryColour.setTryColour(2);
									
								} else if (counter == 3) {
									
									triesColourButton_5_3.setBackground(Color.GREEN);
									checkThirdTryColour.setTryColour(3);
									
								} else if (counter == 4) {
									
									triesColourButton_5_3.setBackground(Color.YELLOW);
									checkThirdTryColour.setTryColour(4);
									
								} else if (counter == 5) {
									
									triesColourButton_5_3.setBackground(Color.MAGENTA);
									checkThirdTryColour.setTryColour(5);
									
								} else if (counter == 6) {
									
									triesColourButton_5_3.setBackground(Color.LIGHT_GRAY);
									checkThirdTryColour.setTryColour(6);
									counter = 0;
									
								}
								
							}
							
						});
						
						triesColourButton_5_4.addActionListener(new ActionListener() {
							
							int counter = 0;
							
							public void actionPerformed (ActionEvent e) {
								
								counter++;
								
								if (counter == 1) {
									
									triesColourButton_5_4.setBackground(Color.BLUE);
									checkFourthTryColour.setTryColour(1);
									
								} else if (counter == 2) {
									
									triesColourButton_5_4.setBackground(Color.RED);
									checkFourthTryColour.setTryColour(2);
									
								} else if (counter == 3) {
									
									triesColourButton_5_4.setBackground(Color.GREEN);
									checkFourthTryColour.setTryColour(3);
									
								} else if (counter == 4) {
									
									triesColourButton_5_4.setBackground(Color.YELLOW);
									checkFourthTryColour.setTryColour(4);
									
								} else if (counter == 5) {
									
									triesColourButton_5_4.setBackground(Color.MAGENTA);
									checkFourthTryColour.setTryColour(5);
									
								} else if (counter == 6) {
									
									triesColourButton_5_4.setBackground(Color.LIGHT_GRAY);
									checkFourthTryColour.setTryColour(6);
									counter = 0;
									
								}
								
							}
							
						});
						
					}
					
				});
				
				checkButton_5.addActionListener(new ActionListener() {
					
					public void actionPerformed (ActionEvent e) {
						
						checkButton_5.setEnabled(false);
						checkButton_6.setEnabled(true);
						
						int countHits = 0;
						int countBlows = 0;
						
						if (checkFirstCombinationColour.getCombinationColour() == 1 && checkFirstTryColour.getTryColour() == 1) {
							
							hitsColourButton_5_1.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 1 && checkFirstTryColour.getTryColour() == 1) {
							
							hitsColourButton_5_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 1 && checkFirstTryColour.getTryColour() == 1) {
							
							hitsColourButton_5_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 1 && checkFirstTryColour.getTryColour() == 1) {
							
							hitsColourButton_5_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 2 && checkFirstTryColour.getTryColour() == 2) {
							
							hitsColourButton_5_1.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 2 && checkFirstTryColour.getTryColour() == 2) {
							
							hitsColourButton_5_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 2 && checkFirstTryColour.getTryColour() == 2) {
							
							hitsColourButton_5_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 2 && checkFirstTryColour.getTryColour() == 2) {
							
							hitsColourButton_5_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 3 && checkFirstTryColour.getTryColour() == 3) {
							
							hitsColourButton_5_1.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 3 && checkFirstTryColour.getTryColour() == 3) {
							
							hitsColourButton_5_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 3 && checkFirstTryColour.getTryColour() == 3) {
							
							hitsColourButton_5_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 3 && checkFirstTryColour.getTryColour() == 3) {
							
							hitsColourButton_5_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 4 && checkFirstTryColour.getTryColour() == 4) {
							
							hitsColourButton_5_1.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 4 && checkFirstTryColour.getTryColour() == 4) {
							
							hitsColourButton_5_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 4 && checkFirstTryColour.getTryColour() == 4) {
							
							hitsColourButton_5_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 4 && checkFirstTryColour.getTryColour() == 4) {
							
							hitsColourButton_5_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 5 && checkFirstTryColour.getTryColour() == 5) {
							
							hitsColourButton_5_1.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 5 && checkFirstTryColour.getTryColour() == 5) {
							
							hitsColourButton_5_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 5 && checkFirstTryColour.getTryColour() == 5) {
							
							hitsColourButton_5_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 5 && checkFirstTryColour.getTryColour() == 5) {
							
							hitsColourButton_5_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 6 && checkFirstTryColour.getTryColour() == 6) {
							
							hitsColourButton_5_1.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 6 && checkFirstTryColour.getTryColour() == 6) {
							
							hitsColourButton_5_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 6 && checkFirstTryColour.getTryColour() == 6) {
							
							hitsColourButton_5_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 6 && checkFirstTryColour.getTryColour() == 6) {
							
							hitsColourButton_5_1.setBackground(Color.WHITE);
							countBlows++;
							
						}
						
						if (checkSecondCombinationColour.getCombinationColour() == 1 && checkSecondTryColour.getTryColour() == 1) {
							
							hitsColourButton_5_2.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 1 && checkSecondTryColour.getTryColour() == 1) {
							
							hitsColourButton_5_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 1 && checkSecondTryColour.getTryColour() == 1) {
							
							hitsColourButton_5_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 1 && checkSecondTryColour.getTryColour() == 1) {
							
							hitsColourButton_5_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 2 && checkSecondTryColour.getTryColour() == 2) {
							
							hitsColourButton_5_2.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 2 && checkSecondTryColour.getTryColour() == 2) {
							
							hitsColourButton_5_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 2 && checkSecondTryColour.getTryColour() == 2) {
							
							hitsColourButton_5_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 2 && checkSecondTryColour.getTryColour() == 2) {
							
							hitsColourButton_5_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 3 && checkSecondTryColour.getTryColour() == 3) {
							
							hitsColourButton_5_2.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 3 && checkSecondTryColour.getTryColour() == 3) {
							
							hitsColourButton_5_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 3 && checkSecondTryColour.getTryColour() == 3) {
							
							hitsColourButton_5_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 3 && checkSecondTryColour.getTryColour() == 3) {
							
							hitsColourButton_5_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 4 && checkSecondTryColour.getTryColour() == 4) {
							
							hitsColourButton_5_2.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 4 && checkSecondTryColour.getTryColour() == 4) {
							
							hitsColourButton_5_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 4 && checkSecondTryColour.getTryColour() == 4) {
							
							hitsColourButton_5_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 4 && checkSecondTryColour.getTryColour() == 4) {
							
							hitsColourButton_5_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 5 && checkSecondTryColour.getTryColour() == 5) {
							
							hitsColourButton_5_2.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 5 && checkSecondTryColour.getTryColour() == 5) {
							
							hitsColourButton_5_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 5 && checkSecondTryColour.getTryColour() == 5) {
							
							hitsColourButton_5_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 5 && checkSecondTryColour.getTryColour() == 5) {
							
							hitsColourButton_5_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 6 && checkSecondTryColour.getTryColour() == 6) {
							
							hitsColourButton_5_2.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 6 && checkSecondTryColour.getTryColour() == 6) {
							
							hitsColourButton_5_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 6 && checkSecondTryColour.getTryColour() == 6) {
							
							hitsColourButton_5_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 6 && checkSecondTryColour.getTryColour() == 6) {
							
							hitsColourButton_5_2.setBackground(Color.WHITE);
							countBlows++;
							
						}
						
						if (checkThirdCombinationColour.getCombinationColour() == 1 && checkThirdTryColour.getTryColour() == 1) {
							
							hitsColourButton_5_3.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 1 && checkThirdTryColour.getTryColour() == 1) {
							
							hitsColourButton_5_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 1 && checkThirdTryColour.getTryColour() == 1) {
							
							hitsColourButton_5_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 1 && checkThirdTryColour.getTryColour() == 1) {
							
							hitsColourButton_5_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 2 && checkThirdTryColour.getTryColour() == 2) {
							
							hitsColourButton_5_3.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 2 && checkThirdTryColour.getTryColour() == 2) {
							
							hitsColourButton_5_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 2 && checkThirdTryColour.getTryColour() == 2) {
							
							hitsColourButton_5_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 2 && checkThirdTryColour.getTryColour() == 2) {
							
							hitsColourButton_5_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 3 && checkThirdTryColour.getTryColour() == 3) {
							
							hitsColourButton_5_3.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 3 && checkThirdTryColour.getTryColour() == 3) {
							
							hitsColourButton_5_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 3 && checkThirdTryColour.getTryColour() == 3) {
							
							hitsColourButton_5_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 3 && checkThirdTryColour.getTryColour() == 3) {
							
							hitsColourButton_5_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 4 && checkThirdTryColour.getTryColour() == 4) {
							
							hitsColourButton_5_3.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 4 && checkThirdTryColour.getTryColour() == 4) {
							
							hitsColourButton_5_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 4 && checkThirdTryColour.getTryColour() == 4) {
							
							hitsColourButton_5_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 4 && checkThirdTryColour.getTryColour() == 4) {
							
							hitsColourButton_5_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 5 && checkThirdTryColour.getTryColour() == 5) {
							
							hitsColourButton_5_3.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 5 && checkThirdTryColour.getTryColour() == 5) {
							
							hitsColourButton_5_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 5 && checkThirdTryColour.getTryColour() == 5) {
							
							hitsColourButton_5_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 5 && checkThirdTryColour.getTryColour() == 5) {
							
							hitsColourButton_5_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 6 && checkThirdTryColour.getTryColour() == 6) {
							
							hitsColourButton_5_3.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 6 && checkThirdTryColour.getTryColour() == 6) {
							
							hitsColourButton_5_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 6 && checkThirdTryColour.getTryColour() == 6) {
							
							hitsColourButton_5_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 6 && checkThirdTryColour.getTryColour() == 6) {
							
							hitsColourButton_5_3.setBackground(Color.WHITE);
							countBlows++;
							
						}
						
						if (checkFourthCombinationColour.getCombinationColour() == 1 && checkFourthTryColour.getTryColour() == 1) {
							
							hitsColourButton_5_4.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 1 && checkFourthTryColour.getTryColour() == 1) {
							
							hitsColourButton_5_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 1 && checkFourthTryColour.getTryColour() == 1) {
							
							hitsColourButton_5_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 1 && checkFourthTryColour.getTryColour() == 1) {
							
							hitsColourButton_5_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 2 && checkFourthTryColour.getTryColour() == 2) {
							
							hitsColourButton_5_4.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 2 && checkFourthTryColour.getTryColour() == 2) {
							
							hitsColourButton_5_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 2 && checkFourthTryColour.getTryColour() == 2) {
							
							hitsColourButton_5_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 2 && checkFourthTryColour.getTryColour() == 2) {
							
							hitsColourButton_5_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 3 && checkFourthTryColour.getTryColour() == 3) {
							
							hitsColourButton_5_4.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 3 && checkFourthTryColour.getTryColour() == 3) {
							
							hitsColourButton_5_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 3 && checkFourthTryColour.getTryColour() == 3) {
							
							hitsColourButton_5_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 3 && checkFourthTryColour.getTryColour() == 3) {
							
							hitsColourButton_5_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 4 && checkFourthTryColour.getTryColour() == 4) {
							
							hitsColourButton_5_4.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 4 && checkFourthTryColour.getTryColour() == 4) {
							
							hitsColourButton_5_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 4 && checkFourthTryColour.getTryColour() == 4) {
							
							hitsColourButton_5_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 4 && checkFourthTryColour.getTryColour() == 4) {
							
							hitsColourButton_5_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 5 && checkFourthTryColour.getTryColour() == 5) {
							
							hitsColourButton_5_4.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 5 && checkFourthTryColour.getTryColour() == 5) {
							
							hitsColourButton_5_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 5 && checkFourthTryColour.getTryColour() == 5) {
							
							hitsColourButton_5_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 5 && checkFourthTryColour.getTryColour() == 5) {
							
							hitsColourButton_5_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 6 && checkFourthTryColour.getTryColour() == 6) {
							
							hitsColourButton_5_4.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 6 && checkFourthTryColour.getTryColour() == 6) {
							
							hitsColourButton_5_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 6 && checkFourthTryColour.getTryColour() == 6) {
							
							hitsColourButton_5_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 6 && checkFourthTryColour.getTryColour() == 6) {
							
							hitsColourButton_5_4.setBackground(Color.WHITE);
							countBlows++;
							
						}
						
						if (countHits > 1 && countHits < 4 && countBlows > 1) {
							
							JOptionPane.showMessageDialog(null, countHits + " Hits! " + countBlows + " Blows!");
							
						} else if (countHits > 1 && countHits < 4 && countBlows == 1) {
							
							JOptionPane.showMessageDialog(null, countHits + " Hits! " + countBlows + " Blow!");
							
						} else if (countHits == 1 && countBlows > 1) {
							
							JOptionPane.showMessageDialog(null, countHits + " Hit! " + countBlows + " Blows!");
							
						} else if (countHits == 1 && countBlows == 1) {
							
							JOptionPane.showMessageDialog(null, countHits + " Hit! " + countBlows + " Blow!");
							
						} else if (countHits > 1 && countHits < 4 && countBlows < 1) {
							
							JOptionPane.showMessageDialog(null, countHits + " Hits! " + countBlows + " Blows!");
							
						} else if (countHits == 1 && countBlows < 1) {
							
							JOptionPane.showMessageDialog(null, countHits + " Hit! " + countBlows + " Blows!");
							
						} else if (countHits < 1 && countBlows > 1) {
							
							JOptionPane.showMessageDialog(null, countHits + " Hits! " + countBlows + " Blows!");
							
						} else if (countHits < 1 && countBlows == 1) {
							
							JOptionPane.showMessageDialog(null, countHits + " Hits! " + countBlows + " Blow!");
							
						} else if (countHits < 1 && countBlows < 1) {
							
							JOptionPane.showMessageDialog(null, countHits + " Hits! " + countBlows + " Blows!");
							
						} else if (countHits == 4) {
							
							checkButton_6.setEnabled(false);
							
							JOptionPane.showMessageDialog(null, "You won the game!");
							
						}
						
						triesColourButton_6_1.addActionListener(new ActionListener() {
							
							int counter = 0;
							
							public void actionPerformed (ActionEvent e) {
								
								counter++;
								
								if (counter == 1) {
									
									triesColourButton_6_1.setBackground(Color.BLUE);
									checkFirstTryColour.setTryColour(1);
									
								} else if (counter == 2) {
									
									triesColourButton_6_1.setBackground(Color.RED);
									checkFirstTryColour.setTryColour(2);
									
								} else if (counter == 3) {
									
									triesColourButton_6_1.setBackground(Color.GREEN);
									checkFirstTryColour.setTryColour(3);
									
								} else if (counter == 4) {
									
									triesColourButton_6_1.setBackground(Color.YELLOW);
									checkFirstTryColour.setTryColour(4);
									
								} else if (counter == 5) {
									
									triesColourButton_6_1.setBackground(Color.MAGENTA);
									checkFirstTryColour.setTryColour(5);
									
								} else if (counter == 6) {
									
									triesColourButton_6_1.setBackground(Color.LIGHT_GRAY);
									checkFirstTryColour.setTryColour(6);
									counter = 0;
									
								}
								
							}
							
						});

						triesColourButton_6_2.addActionListener(new ActionListener() {
							
							int counter = 0;
							
							public void actionPerformed (ActionEvent e) {
								
								counter++;
								
								if (counter == 1) {
									
									triesColourButton_6_2.setBackground(Color.BLUE);
									checkSecondTryColour.setTryColour(1);
									
								} else if (counter == 2) {
									
									triesColourButton_6_2.setBackground(Color.RED);
									checkSecondTryColour.setTryColour(2);
									
								} else if (counter == 3) {
									
									triesColourButton_6_2.setBackground(Color.GREEN);
									checkSecondTryColour.setTryColour(3);
									
								} else if (counter == 4) {
									
									triesColourButton_6_2.setBackground(Color.YELLOW);
									checkSecondTryColour.setTryColour(4);
									
								} else if (counter == 5) {
									
									triesColourButton_6_2.setBackground(Color.MAGENTA);
									checkSecondTryColour.setTryColour(5);
									
								} else if (counter == 6) {
									
									triesColourButton_6_2.setBackground(Color.LIGHT_GRAY);
									checkSecondTryColour.setTryColour(6);
									counter = 0;
									
								}
								
							}
							
						});

						triesColourButton_6_3.addActionListener(new ActionListener() {
							
							int counter = 0;
							
							public void actionPerformed (ActionEvent e) {
								
								counter++;
								
								if (counter == 1) {
									
									triesColourButton_6_3.setBackground(Color.BLUE);
									checkThirdTryColour.setTryColour(1);
									
								} else if (counter == 2) {
									
									triesColourButton_6_3.setBackground(Color.RED);
									checkThirdTryColour.setTryColour(2);
									
								} else if (counter == 3) {
									
									triesColourButton_6_3.setBackground(Color.GREEN);
									checkThirdTryColour.setTryColour(3);
									
								} else if (counter == 4) {
									
									triesColourButton_6_3.setBackground(Color.YELLOW);
									checkThirdTryColour.setTryColour(4);
									
								} else if (counter == 5) {
									
									triesColourButton_6_3.setBackground(Color.MAGENTA);
									checkThirdTryColour.setTryColour(5);
									
								} else if (counter == 6) {
									
									triesColourButton_6_3.setBackground(Color.LIGHT_GRAY);
									checkThirdTryColour.setTryColour(6);
									counter = 0;
									
								}
								
							}
							
						});

						triesColourButton_6_4.addActionListener(new ActionListener() {
							
							int counter = 0;
							
							public void actionPerformed (ActionEvent e) {
								
								counter++;
								
								if (counter == 1) {
									
									triesColourButton_6_4.setBackground(Color.BLUE);
									checkFourthTryColour.setTryColour(1);
									
								} else if (counter == 2) {
									
									triesColourButton_6_4.setBackground(Color.RED);
									checkFourthTryColour.setTryColour(2);
									
								} else if (counter == 3) {
									
									triesColourButton_6_4.setBackground(Color.GREEN);
									checkFourthTryColour.setTryColour(3);
									
								} else if (counter == 4) {
									
									triesColourButton_6_4.setBackground(Color.YELLOW);
									checkFourthTryColour.setTryColour(4);
									
								} else if (counter == 5) {
									
									triesColourButton_6_4.setBackground(Color.MAGENTA);
									checkFourthTryColour.setTryColour(5);
									
								} else if (counter == 6) {
									
									triesColourButton_6_4.setBackground(Color.LIGHT_GRAY);
									checkFourthTryColour.setTryColour(6);
									counter = 0;
									
								}
								
							}
							
						});
						
					}
					
				});
				
				checkButton_6.addActionListener(new ActionListener() {
					
					public void actionPerformed (ActionEvent e) {
						
						checkButton_6.setEnabled(false);
						checkButton_7.setEnabled(true);
						
						int countHits = 0;
						int countBlows = 0;
						
						if (checkFirstCombinationColour.getCombinationColour() == 1 && checkFirstTryColour.getTryColour() == 1) {
							
							hitsColourButton_6_1.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 1 && checkFirstTryColour.getTryColour() == 1) {
							
							hitsColourButton_6_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 1 && checkFirstTryColour.getTryColour() == 1) {
							
							hitsColourButton_6_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 1 && checkFirstTryColour.getTryColour() == 1) {
							
							hitsColourButton_6_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 2 && checkFirstTryColour.getTryColour() == 2) {
							
							hitsColourButton_6_1.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 2 && checkFirstTryColour.getTryColour() == 2) {
							
							hitsColourButton_6_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 2 && checkFirstTryColour.getTryColour() == 2) {
							
							hitsColourButton_6_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 2 && checkFirstTryColour.getTryColour() == 2) {
							
							hitsColourButton_6_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 3 && checkFirstTryColour.getTryColour() == 3) {
							
							hitsColourButton_6_1.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 3 && checkFirstTryColour.getTryColour() == 3) {
							
							hitsColourButton_6_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 3 && checkFirstTryColour.getTryColour() == 3) {
							
							hitsColourButton_6_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 3 && checkFirstTryColour.getTryColour() == 3) {
							
							hitsColourButton_6_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 4 && checkFirstTryColour.getTryColour() == 4) {
							
							hitsColourButton_6_1.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 4 && checkFirstTryColour.getTryColour() == 4) {
							
							hitsColourButton_6_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 4 && checkFirstTryColour.getTryColour() == 4) {
							
							hitsColourButton_6_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 4 && checkFirstTryColour.getTryColour() == 4) {
							
							hitsColourButton_6_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 5 && checkFirstTryColour.getTryColour() == 5) {
							
							hitsColourButton_6_1.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 5 && checkFirstTryColour.getTryColour() == 5) {
							
							hitsColourButton_6_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 5 && checkFirstTryColour.getTryColour() == 5) {
							
							hitsColourButton_6_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 5 && checkFirstTryColour.getTryColour() == 5) {
							
							hitsColourButton_6_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 6 && checkFirstTryColour.getTryColour() == 6) {
							
							hitsColourButton_6_1.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 6 && checkFirstTryColour.getTryColour() == 6) {
							
							hitsColourButton_6_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 6 && checkFirstTryColour.getTryColour() == 6) {
							
							hitsColourButton_6_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 6 && checkFirstTryColour.getTryColour() == 6) {
							
							hitsColourButton_6_1.setBackground(Color.WHITE);
							countBlows++;
							
						}
						
						if (checkSecondCombinationColour.getCombinationColour() == 1 && checkSecondTryColour.getTryColour() == 1) {
							
							hitsColourButton_6_2.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 1 && checkSecondTryColour.getTryColour() == 1) {
							
							hitsColourButton_6_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 1 && checkSecondTryColour.getTryColour() == 1) {
							
							hitsColourButton_6_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 1 && checkSecondTryColour.getTryColour() == 1) {
							
							hitsColourButton_6_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 2 && checkSecondTryColour.getTryColour() == 2) {
							
							hitsColourButton_6_2.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 2 && checkSecondTryColour.getTryColour() == 2) {
							
							hitsColourButton_6_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 2 && checkSecondTryColour.getTryColour() == 2) {
							
							hitsColourButton_6_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 2 && checkSecondTryColour.getTryColour() == 2) {
							
							hitsColourButton_6_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 3 && checkSecondTryColour.getTryColour() == 3) {
							
							hitsColourButton_6_2.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 3 && checkSecondTryColour.getTryColour() == 3) {
							
							hitsColourButton_6_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 3 && checkSecondTryColour.getTryColour() == 3) {
							
							hitsColourButton_6_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 3 && checkSecondTryColour.getTryColour() == 3) {
							
							hitsColourButton_6_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 4 && checkSecondTryColour.getTryColour() == 4) {
							
							hitsColourButton_6_2.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 4 && checkSecondTryColour.getTryColour() == 4) {
							
							hitsColourButton_6_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 4 && checkSecondTryColour.getTryColour() == 4) {
							
							hitsColourButton_6_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 4 && checkSecondTryColour.getTryColour() == 4) {
							
							hitsColourButton_6_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 5 && checkSecondTryColour.getTryColour() == 5) {
							
							hitsColourButton_6_2.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 5 && checkSecondTryColour.getTryColour() == 5) {
							
							hitsColourButton_6_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 5 && checkSecondTryColour.getTryColour() == 5) {
							
							hitsColourButton_6_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 5 && checkSecondTryColour.getTryColour() == 5) {
							
							hitsColourButton_6_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 6 && checkSecondTryColour.getTryColour() == 6) {
							
							hitsColourButton_6_2.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 6 && checkSecondTryColour.getTryColour() == 6) {
							
							hitsColourButton_6_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 6 && checkSecondTryColour.getTryColour() == 6) {
							
							hitsColourButton_6_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 6 && checkSecondTryColour.getTryColour() == 6) {
							
							hitsColourButton_6_2.setBackground(Color.WHITE);
							countBlows++;
							
						}
						
						if (checkThirdCombinationColour.getCombinationColour() == 1 && checkThirdTryColour.getTryColour() == 1) {
							
							hitsColourButton_6_3.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 1 && checkThirdTryColour.getTryColour() == 1) {
							
							hitsColourButton_6_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 1 && checkThirdTryColour.getTryColour() == 1) {
							
							hitsColourButton_6_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 1 && checkThirdTryColour.getTryColour() == 1) {
							
							hitsColourButton_6_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 2 && checkThirdTryColour.getTryColour() == 2) {
							
							hitsColourButton_6_3.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 2 && checkThirdTryColour.getTryColour() == 2) {
							
							hitsColourButton_6_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 2 && checkThirdTryColour.getTryColour() == 2) {
							
							hitsColourButton_6_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 2 && checkThirdTryColour.getTryColour() == 2) {
							
							hitsColourButton_6_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 3 && checkThirdTryColour.getTryColour() == 3) {
							
							hitsColourButton_6_3.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 3 && checkThirdTryColour.getTryColour() == 3) {
							
							hitsColourButton_6_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 3 && checkThirdTryColour.getTryColour() == 3) {
							
							hitsColourButton_6_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 3 && checkThirdTryColour.getTryColour() == 3) {
							
							hitsColourButton_6_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 4 && checkThirdTryColour.getTryColour() == 4) {
							
							hitsColourButton_6_3.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 4 && checkThirdTryColour.getTryColour() == 4) {
							
							hitsColourButton_6_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 4 && checkThirdTryColour.getTryColour() == 4) {
							
							hitsColourButton_6_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 4 && checkThirdTryColour.getTryColour() == 4) {
							
							hitsColourButton_6_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 5 && checkThirdTryColour.getTryColour() == 5) {
							
							hitsColourButton_6_3.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 5 && checkThirdTryColour.getTryColour() == 5) {
							
							hitsColourButton_6_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 5 && checkThirdTryColour.getTryColour() == 5) {
							
							hitsColourButton_6_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 5 && checkThirdTryColour.getTryColour() == 5) {
							
							hitsColourButton_6_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 6 && checkThirdTryColour.getTryColour() == 6) {
							
							hitsColourButton_6_3.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 6 && checkThirdTryColour.getTryColour() == 6) {
							
							hitsColourButton_6_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 6 && checkThirdTryColour.getTryColour() == 6) {
							
							hitsColourButton_6_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 6 && checkThirdTryColour.getTryColour() == 6) {
							
							hitsColourButton_6_3.setBackground(Color.WHITE);
							countBlows++;
							
						}
						
						if (checkFourthCombinationColour.getCombinationColour() == 1 && checkFourthTryColour.getTryColour() == 1) {
							
							hitsColourButton_6_4.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 1 && checkFourthTryColour.getTryColour() == 1) {
							
							hitsColourButton_6_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 1 && checkFourthTryColour.getTryColour() == 1) {
							
							hitsColourButton_6_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 1 && checkFourthTryColour.getTryColour() == 1) {
							
							hitsColourButton_6_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 2 && checkFourthTryColour.getTryColour() == 2) {
							
							hitsColourButton_6_4.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 2 && checkFourthTryColour.getTryColour() == 2) {
							
							hitsColourButton_6_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 2 && checkFourthTryColour.getTryColour() == 2) {
							
							hitsColourButton_6_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 2 && checkFourthTryColour.getTryColour() == 2) {
							
							hitsColourButton_6_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 3 && checkFourthTryColour.getTryColour() == 3) {
							
							hitsColourButton_6_4.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 3 && checkFourthTryColour.getTryColour() == 3) {
							
							hitsColourButton_6_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 3 && checkFourthTryColour.getTryColour() == 3) {
							
							hitsColourButton_6_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 3 && checkFourthTryColour.getTryColour() == 3) {
							
							hitsColourButton_6_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 4 && checkFourthTryColour.getTryColour() == 4) {
							
							hitsColourButton_6_4.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 4 && checkFourthTryColour.getTryColour() == 4) {
							
							hitsColourButton_6_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 4 && checkFourthTryColour.getTryColour() == 4) {
							
							hitsColourButton_6_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 4 && checkFourthTryColour.getTryColour() == 4) {
							
							hitsColourButton_6_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 5 && checkFourthTryColour.getTryColour() == 5) {
							
							hitsColourButton_6_4.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 5 && checkFourthTryColour.getTryColour() == 5) {
							
							hitsColourButton_6_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 5 && checkFourthTryColour.getTryColour() == 5) {
							
							hitsColourButton_6_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 5 && checkFourthTryColour.getTryColour() == 5) {
							
							hitsColourButton_6_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 6 && checkFourthTryColour.getTryColour() == 6) {
							
							hitsColourButton_6_4.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 6 && checkFourthTryColour.getTryColour() == 6) {
							
							hitsColourButton_6_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 6 && checkFourthTryColour.getTryColour() == 6) {
							
							hitsColourButton_1_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 6 && checkFourthTryColour.getTryColour() == 6) {
							
							hitsColourButton_6_4.setBackground(Color.WHITE);
							countBlows++;
							
						}
						
						if (countHits > 1 && countHits < 4 && countBlows > 1) {
							
							JOptionPane.showMessageDialog(null, countHits + " Hits! " + countBlows + " Blows!");
							
						} else if (countHits > 1 && countHits < 4 && countBlows == 1) {
							
							JOptionPane.showMessageDialog(null, countHits + " Hits! " + countBlows + " Blow!");
							
						} else if (countHits == 1 && countBlows > 1) {
							
							JOptionPane.showMessageDialog(null, countHits + " Hit! " + countBlows + " Blows!");
							
						} else if (countHits == 1 && countBlows == 1) {
							
							JOptionPane.showMessageDialog(null, countHits + " Hit! " + countBlows + " Blow!");
							
						} else if (countHits > 1 && countHits < 4 && countBlows < 1) {
							
							JOptionPane.showMessageDialog(null, countHits + " Hits! " + countBlows + " Blows!");
							
						} else if (countHits == 1 && countBlows < 1) {
							
							JOptionPane.showMessageDialog(null, countHits + " Hit! " + countBlows + " Blows!");
							
						} else if (countHits < 1 && countBlows > 1) {
							
							JOptionPane.showMessageDialog(null, countHits + " Hits! " + countBlows + " Blows!");
							
						} else if (countHits < 1 && countBlows == 1) {
							
							JOptionPane.showMessageDialog(null, countHits + " Hits! " + countBlows + " Blow!");
							
						} else if (countHits < 1 && countBlows < 1) {
							
							JOptionPane.showMessageDialog(null, countHits + " Hits! " + countBlows + " Blows!");
							
						} else if (countHits == 4) {
							
							checkButton_7.setEnabled(false);
							
							JOptionPane.showMessageDialog(null, "You won the game!");
							
						}

						triesColourButton_7_1.addActionListener(new ActionListener() {
							
							int counter = 0;
							
							public void actionPerformed (ActionEvent e) {
								
								counter++;
								
								if (counter == 1) {
									
									triesColourButton_7_1.setBackground(Color.BLUE);
									checkFirstTryColour.setTryColour(1);
									
								} else if (counter == 2) {
									
									triesColourButton_7_1.setBackground(Color.RED);
									checkFirstTryColour.setTryColour(2);
									
								} else if (counter == 3) {
									
									triesColourButton_7_1.setBackground(Color.GREEN);
									checkFirstTryColour.setTryColour(3);
									
								} else if (counter == 4) {
									
									triesColourButton_7_1.setBackground(Color.YELLOW);
									checkFirstTryColour.setTryColour(4);
									
								} else if (counter == 5) {
									
									triesColourButton_7_1.setBackground(Color.MAGENTA);
									checkFirstTryColour.setTryColour(5);
									
								} else if (counter == 6) {
									
									triesColourButton_7_1.setBackground(Color.LIGHT_GRAY);
									checkFirstTryColour.setTryColour(6);
									counter = 0;
									
								}
								
							}
							
						});

						triesColourButton_7_2.addActionListener(new ActionListener() {
							
							int counter = 0;
							
							public void actionPerformed (ActionEvent e) {
								
								counter++;
								
								if (counter == 1) {
									
									triesColourButton_7_2.setBackground(Color.BLUE);
									checkSecondTryColour.setTryColour(1);
									
								} else if (counter == 2) {
									
									triesColourButton_7_2.setBackground(Color.RED);
									checkSecondTryColour.setTryColour(2);
									
								} else if (counter == 3) {
									
									triesColourButton_7_2.setBackground(Color.GREEN);
									checkSecondTryColour.setTryColour(3);
									
								} else if (counter == 4) {
									
									triesColourButton_7_2.setBackground(Color.YELLOW);
									checkSecondTryColour.setTryColour(4);
									
								} else if (counter == 5) {
									
									triesColourButton_7_2.setBackground(Color.MAGENTA);
									checkSecondTryColour.setTryColour(5);
									
								} else if (counter == 6) {
									
									triesColourButton_7_2.setBackground(Color.LIGHT_GRAY);
									checkSecondTryColour.setTryColour(6);
									counter = 0;
									
								}
								
							}
							
						});
						
						triesColourButton_7_3.addActionListener(new ActionListener() {
							
							int counter = 0;
							
							public void actionPerformed (ActionEvent e) {
								
								counter++;
								
								if (counter == 1) {
									
									triesColourButton_7_3.setBackground(Color.BLUE);
									checkThirdTryColour.setTryColour(1);
									
								} else if (counter == 2) {
									
									triesColourButton_7_3.setBackground(Color.RED);
									checkThirdTryColour.setTryColour(2);
									
								} else if (counter == 3) {
									
									triesColourButton_7_3.setBackground(Color.GREEN);
									checkThirdTryColour.setTryColour(3);
									
								} else if (counter == 4) {
									
									triesColourButton_7_3.setBackground(Color.YELLOW);
									checkThirdTryColour.setTryColour(4);
									
								} else if (counter == 5) {
									
									triesColourButton_7_3.setBackground(Color.MAGENTA);
									checkThirdTryColour.setTryColour(5);
									
								} else if (counter == 6) {
									
									triesColourButton_7_3.setBackground(Color.LIGHT_GRAY);
									checkThirdTryColour.setTryColour(6);
									counter = 0;
									
								}
								
							}
							
						});

						triesColourButton_7_4.addActionListener(new ActionListener() {
							
							int counter = 0;
							
							public void actionPerformed (ActionEvent e) {
								
								counter++;
								
								if (counter == 1) {
									
									triesColourButton_7_4.setBackground(Color.BLUE);
									checkFourthTryColour.setTryColour(1);
									
								} else if (counter == 2) {
									
									triesColourButton_7_4.setBackground(Color.RED);
									checkFourthTryColour.setTryColour(2);
									
								} else if (counter == 3) {
									
									triesColourButton_7_4.setBackground(Color.GREEN);
									checkFourthTryColour.setTryColour(3);
									
								} else if (counter == 4) {
									
									triesColourButton_7_4.setBackground(Color.YELLOW);
									checkFourthTryColour.setTryColour(4);
									
								} else if (counter == 5) {
									
									triesColourButton_7_4.setBackground(Color.MAGENTA);
									checkFourthTryColour.setTryColour(5);
									
								} else if (counter == 6) {
									
									triesColourButton_7_4.setBackground(Color.LIGHT_GRAY);
									checkFourthTryColour.setTryColour(6);
									counter = 0;
									
								}
								
							}
							
						});
						
					}
					
				});
				
				checkButton_7.addActionListener(new ActionListener() {
					
					public void actionPerformed (ActionEvent e) {
						
						checkButton_7.setEnabled(false);
						checkButton_8.setEnabled(true);
						
						int countHits = 0;
						int countBlows = 0;
						
						if (checkFirstCombinationColour.getCombinationColour() == 1 && checkFirstTryColour.getTryColour() == 1) {
							
							hitsColourButton_7_1.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 1 && checkFirstTryColour.getTryColour() == 1) {
							
							hitsColourButton_7_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 1 && checkFirstTryColour.getTryColour() == 1) {
							
							hitsColourButton_7_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 1 && checkFirstTryColour.getTryColour() == 1) {
							
							hitsColourButton_7_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 2 && checkFirstTryColour.getTryColour() == 2) {
							
							hitsColourButton_7_1.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 2 && checkFirstTryColour.getTryColour() == 2) {
							
							hitsColourButton_7_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 2 && checkFirstTryColour.getTryColour() == 2) {
							
							hitsColourButton_7_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 2 && checkFirstTryColour.getTryColour() == 2) {
							
							hitsColourButton_7_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 3 && checkFirstTryColour.getTryColour() == 3) {
							
							hitsColourButton_7_1.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 3 && checkFirstTryColour.getTryColour() == 3) {
							
							hitsColourButton_7_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 3 && checkFirstTryColour.getTryColour() == 3) {
							
							hitsColourButton_7_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 3 && checkFirstTryColour.getTryColour() == 3) {
							
							hitsColourButton_7_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 4 && checkFirstTryColour.getTryColour() == 4) {
							
							hitsColourButton_7_1.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 4 && checkFirstTryColour.getTryColour() == 4) {
							
							hitsColourButton_7_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 4 && checkFirstTryColour.getTryColour() == 4) {
							
							hitsColourButton_7_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 4 && checkFirstTryColour.getTryColour() == 4) {
							
							hitsColourButton_7_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 5 && checkFirstTryColour.getTryColour() == 5) {
							
							hitsColourButton_7_1.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 5 && checkFirstTryColour.getTryColour() == 5) {
							
							hitsColourButton_7_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 5 && checkFirstTryColour.getTryColour() == 5) {
							
							hitsColourButton_7_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 5 && checkFirstTryColour.getTryColour() == 5) {
							
							hitsColourButton_7_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 6 && checkFirstTryColour.getTryColour() == 6) {
							
							hitsColourButton_7_1.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 6 && checkFirstTryColour.getTryColour() == 6) {
							
							hitsColourButton_7_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 6 && checkFirstTryColour.getTryColour() == 6) {
							
							hitsColourButton_7_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 6 && checkFirstTryColour.getTryColour() == 6) {
							
							hitsColourButton_7_1.setBackground(Color.WHITE);
							countBlows++;
							
						}
						
						if (checkSecondCombinationColour.getCombinationColour() == 1 && checkSecondTryColour.getTryColour() == 1) {
							
							hitsColourButton_7_2.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 1 && checkSecondTryColour.getTryColour() == 1) {
							
							hitsColourButton_7_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 1 && checkSecondTryColour.getTryColour() == 1) {
							
							hitsColourButton_7_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 1 && checkSecondTryColour.getTryColour() == 1) {
							
							hitsColourButton_7_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 2 && checkSecondTryColour.getTryColour() == 2) {
							
							hitsColourButton_7_2.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 2 && checkSecondTryColour.getTryColour() == 2) {
							
							hitsColourButton_7_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 2 && checkSecondTryColour.getTryColour() == 2) {
							
							hitsColourButton_7_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 2 && checkSecondTryColour.getTryColour() == 2) {
							
							hitsColourButton_7_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 3 && checkSecondTryColour.getTryColour() == 3) {
							
							hitsColourButton_7_2.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 3 && checkSecondTryColour.getTryColour() == 3) {
							
							hitsColourButton_7_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 3 && checkSecondTryColour.getTryColour() == 3) {
							
							hitsColourButton_7_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 3 && checkSecondTryColour.getTryColour() == 3) {
							
							hitsColourButton_7_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 4 && checkSecondTryColour.getTryColour() == 4) {
							
							hitsColourButton_7_2.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 4 && checkSecondTryColour.getTryColour() == 4) {
							
							hitsColourButton_7_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 4 && checkSecondTryColour.getTryColour() == 4) {
							
							hitsColourButton_7_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 4 && checkSecondTryColour.getTryColour() == 4) {
							
							hitsColourButton_7_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 5 && checkSecondTryColour.getTryColour() == 5) {
							
							hitsColourButton_7_2.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 5 && checkSecondTryColour.getTryColour() == 5) {
							
							hitsColourButton_7_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 5 && checkSecondTryColour.getTryColour() == 5) {
							
							hitsColourButton_7_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 5 && checkSecondTryColour.getTryColour() == 5) {
							
							hitsColourButton_7_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 6 && checkSecondTryColour.getTryColour() == 6) {
							
							hitsColourButton_7_2.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 6 && checkSecondTryColour.getTryColour() == 6) {
							
							hitsColourButton_7_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 6 && checkSecondTryColour.getTryColour() == 6) {
							
							hitsColourButton_7_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 6 && checkSecondTryColour.getTryColour() == 6) {
							
							hitsColourButton_7_2.setBackground(Color.WHITE);
							countBlows++;
							
						}
						
						if (checkThirdCombinationColour.getCombinationColour() == 1 && checkThirdTryColour.getTryColour() == 1) {
							
							hitsColourButton_7_3.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 1 && checkThirdTryColour.getTryColour() == 1) {
							
							hitsColourButton_7_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 1 && checkThirdTryColour.getTryColour() == 1) {
							
							hitsColourButton_7_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 1 && checkThirdTryColour.getTryColour() == 1) {
							
							hitsColourButton_7_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 2 && checkThirdTryColour.getTryColour() == 2) {
							
							hitsColourButton_7_3.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 2 && checkThirdTryColour.getTryColour() == 2) {
							
							hitsColourButton_7_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 2 && checkThirdTryColour.getTryColour() == 2) {
							
							hitsColourButton_7_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 2 && checkThirdTryColour.getTryColour() == 2) {
							
							hitsColourButton_7_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 3 && checkThirdTryColour.getTryColour() == 3) {
							
							hitsColourButton_7_3.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 3 && checkThirdTryColour.getTryColour() == 3) {
							
							hitsColourButton_7_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 3 && checkThirdTryColour.getTryColour() == 3) {
							
							hitsColourButton_7_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 3 && checkThirdTryColour.getTryColour() == 3) {
							
							hitsColourButton_7_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 4 && checkThirdTryColour.getTryColour() == 4) {
							
							hitsColourButton_7_3.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 4 && checkThirdTryColour.getTryColour() == 4) {
							
							hitsColourButton_7_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 4 && checkThirdTryColour.getTryColour() == 4) {
							
							hitsColourButton_7_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 4 && checkThirdTryColour.getTryColour() == 4) {
							
							hitsColourButton_7_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 5 && checkThirdTryColour.getTryColour() == 5) {
							
							hitsColourButton_7_3.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 5 && checkThirdTryColour.getTryColour() == 5) {
							
							hitsColourButton_7_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 5 && checkThirdTryColour.getTryColour() == 5) {
							
							hitsColourButton_7_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 5 && checkThirdTryColour.getTryColour() == 5) {
							
							hitsColourButton_7_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 6 && checkThirdTryColour.getTryColour() == 6) {
							
							hitsColourButton_7_3.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 6 && checkThirdTryColour.getTryColour() == 6) {
							
							hitsColourButton_7_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 6 && checkThirdTryColour.getTryColour() == 6) {
							
							hitsColourButton_7_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 6 && checkThirdTryColour.getTryColour() == 6) {
							
							hitsColourButton_7_3.setBackground(Color.WHITE);
							countBlows++;
							
						}
						
						if (checkFourthCombinationColour.getCombinationColour() == 1 && checkFourthTryColour.getTryColour() == 1) {
							
							hitsColourButton_7_4.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 1 && checkFourthTryColour.getTryColour() == 1) {
							
							hitsColourButton_7_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 1 && checkFourthTryColour.getTryColour() == 1) {
							
							hitsColourButton_7_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 1 && checkFourthTryColour.getTryColour() == 1) {
							
							hitsColourButton_7_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 2 && checkFourthTryColour.getTryColour() == 2) {
							
							hitsColourButton_7_4.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 2 && checkFourthTryColour.getTryColour() == 2) {
							
							hitsColourButton_7_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 2 && checkFourthTryColour.getTryColour() == 2) {
							
							hitsColourButton_7_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 2 && checkFourthTryColour.getTryColour() == 2) {
							
							hitsColourButton_7_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 3 && checkFourthTryColour.getTryColour() == 3) {
							
							hitsColourButton_7_4.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 3 && checkFourthTryColour.getTryColour() == 3) {
							
							hitsColourButton_7_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 3 && checkFourthTryColour.getTryColour() == 3) {
							
							hitsColourButton_7_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 3 && checkFourthTryColour.getTryColour() == 3) {
							
							hitsColourButton_7_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 4 && checkFourthTryColour.getTryColour() == 4) {
							
							hitsColourButton_7_4.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 4 && checkFourthTryColour.getTryColour() == 4) {
							
							hitsColourButton_7_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 4 && checkFourthTryColour.getTryColour() == 4) {
							
							hitsColourButton_7_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 4 && checkFourthTryColour.getTryColour() == 4) {
							
							hitsColourButton_7_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 5 && checkFourthTryColour.getTryColour() == 5) {
							
							hitsColourButton_7_4.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 5 && checkFourthTryColour.getTryColour() == 5) {
							
							hitsColourButton_7_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 5 && checkFourthTryColour.getTryColour() == 5) {
							
							hitsColourButton_7_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 5 && checkFourthTryColour.getTryColour() == 5) {
							
							hitsColourButton_7_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 6 && checkFourthTryColour.getTryColour() == 6) {
							
							hitsColourButton_7_4.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 6 && checkFourthTryColour.getTryColour() == 6) {
							
							hitsColourButton_7_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 6 && checkFourthTryColour.getTryColour() == 6) {
							
							hitsColourButton_7_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 6 && checkFourthTryColour.getTryColour() == 6) {
							
							hitsColourButton_7_4.setBackground(Color.WHITE);
							countBlows++;
							
						}
						
						if (countHits > 1 && countHits < 4 && countBlows > 1) {
							
							JOptionPane.showMessageDialog(null, countHits + " Hits! " + countBlows + " Blows!");
							
						} else if (countHits > 1 && countHits < 4 && countBlows == 1) {
							
							JOptionPane.showMessageDialog(null, countHits + " Hits! " + countBlows + " Blow!");
							
						} else if (countHits == 1 && countBlows > 1) {
							
							JOptionPane.showMessageDialog(null, countHits + " Hit! " + countBlows + " Blows!");
							
						} else if (countHits == 1 && countBlows == 1) {
							
							JOptionPane.showMessageDialog(null, countHits + " Hit! " + countBlows + " Blow!");
							
						} else if (countHits > 1 && countHits < 4 && countBlows < 1) {
							
							JOptionPane.showMessageDialog(null, countHits + " Hits! " + countBlows + " Blows!");
							
						} else if (countHits == 1 && countBlows < 1) {
							
							JOptionPane.showMessageDialog(null, countHits + " Hit! " + countBlows + " Blows!");
							
						} else if (countHits < 1 && countBlows > 1) {
							
							JOptionPane.showMessageDialog(null, countHits + " Hits! " + countBlows + " Blows!");
							
						} else if (countHits < 1 && countBlows == 1) {
							
							JOptionPane.showMessageDialog(null, countHits + " Hits! " + countBlows + " Blow!");
							
						} else if (countHits < 1 && countBlows < 1) {
							
							JOptionPane.showMessageDialog(null, countHits + " Hits! " + countBlows + " Blows!");
							
						} else if (countHits == 4) {
							
							checkButton_8.setEnabled(false);
							
							JOptionPane.showMessageDialog(null, "You won the game!");
							
						}
						
						triesColourButton_8_1.addActionListener(new ActionListener() {
							
							int counter = 0;
							
							public void actionPerformed (ActionEvent e) {
								
								counter++;
								
								if (counter == 1) {
									
									triesColourButton_8_1.setBackground(Color.BLUE);
									checkFirstTryColour.setTryColour(1);
									
								} else if (counter == 2) {
									
									triesColourButton_8_1.setBackground(Color.RED);
									checkFirstTryColour.setTryColour(2);
									
								} else if (counter == 3) {
									
									triesColourButton_8_1.setBackground(Color.GREEN);
									checkFirstTryColour.setTryColour(3);
									
								} else if (counter == 4) {
									
									triesColourButton_8_1.setBackground(Color.YELLOW);
									checkFirstTryColour.setTryColour(4);
									
								} else if (counter == 5) {
									
									triesColourButton_8_1.setBackground(Color.MAGENTA);
									checkFirstTryColour.setTryColour(5);
									
								} else if (counter == 6) {
									
									triesColourButton_8_1.setBackground(Color.LIGHT_GRAY);
									checkFirstTryColour.setTryColour(6);
									counter = 0;
									
								}
								
							}
							
						});

						triesColourButton_8_2.addActionListener(new ActionListener() {
							
							int counter = 0;
							
							public void actionPerformed (ActionEvent e) {
								
								counter++;
								
								if (counter == 1) {
									
									triesColourButton_8_2.setBackground(Color.BLUE);
									checkSecondTryColour.setTryColour(1);
									
								} else if (counter == 2) {
									
									triesColourButton_8_2.setBackground(Color.RED);
									checkSecondTryColour.setTryColour(2);
									
								} else if (counter == 3) {
									
									triesColourButton_8_2.setBackground(Color.GREEN);
									checkSecondTryColour.setTryColour(3);
									
								} else if (counter == 4) {
									
									triesColourButton_8_2.setBackground(Color.YELLOW);
									checkSecondTryColour.setTryColour(4);
									
								} else if (counter == 5) {
									
									triesColourButton_8_2.setBackground(Color.MAGENTA);
									checkSecondTryColour.setTryColour(5);
									
								} else if (counter == 6) {
									
									triesColourButton_8_2.setBackground(Color.LIGHT_GRAY);
									checkSecondTryColour.setTryColour(6);
									counter = 0;
									
								}
								
							}
							
						});

						triesColourButton_8_3.addActionListener(new ActionListener() {
							
							int counter = 0;
							
							public void actionPerformed (ActionEvent e) {
								
								counter++;
								
								if (counter == 1) {
									
									triesColourButton_8_3.setBackground(Color.BLUE);
									checkThirdTryColour.setTryColour(1);
									
								} else if (counter == 2) {
									
									triesColourButton_8_3.setBackground(Color.RED);
									checkThirdTryColour.setTryColour(2);
									
								} else if (counter == 3) {
									
									triesColourButton_8_3.setBackground(Color.GREEN);
									checkThirdTryColour.setTryColour(3);
									
								} else if (counter == 4) {
									
									triesColourButton_8_3.setBackground(Color.YELLOW);
									checkThirdTryColour.setTryColour(4);
									
								} else if (counter == 5) {
									
									triesColourButton_8_3.setBackground(Color.MAGENTA);
									checkThirdTryColour.setTryColour(5);
									
								} else if (counter == 6) {
									
									triesColourButton_8_3.setBackground(Color.LIGHT_GRAY);
									checkThirdTryColour.setTryColour(6);
									counter = 0;
									
								}
								
							}
							
						});

						triesColourButton_8_4.addActionListener(new ActionListener() {
							
							int counter = 0;
							
							public void actionPerformed (ActionEvent e) {
								
								counter++;
								
								if (counter == 1) {
									
									triesColourButton_8_4.setBackground(Color.BLUE);
									checkFourthTryColour.setTryColour(1);
									
								} else if (counter == 2) {
									
									triesColourButton_8_4.setBackground(Color.RED);
									checkFourthTryColour.setTryColour(2);
									
								} else if (counter == 3) {
									
									triesColourButton_8_4.setBackground(Color.GREEN);
									checkFourthTryColour.setTryColour(3);
									
								} else if (counter == 4) {
									
									triesColourButton_8_4.setBackground(Color.YELLOW);
									checkFourthTryColour.setTryColour(4);
									
								} else if (counter == 5) {
									
									triesColourButton_8_4.setBackground(Color.MAGENTA);
									checkFourthTryColour.setTryColour(5);
									
								} else if (counter == 6) {
									
									triesColourButton_8_4.setBackground(Color.LIGHT_GRAY);
									checkFourthTryColour.setTryColour(6);
									counter = 0;
									
								}
								
							}
							
						});
						
					}
					
				});
				
				checkButton_8.addActionListener(new ActionListener() {
					
					public void actionPerformed (ActionEvent e) {
						
						checkButton_8.setEnabled(false);
						
						int countHits = 0;
						int countBlows = 0;
						
						if (checkFirstCombinationColour.getCombinationColour() == 1 && checkFirstTryColour.getTryColour() == 1) {
							
							hitsColourButton_8_1.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 1 && checkFirstTryColour.getTryColour() == 1) {
							
							hitsColourButton_8_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 1 && checkFirstTryColour.getTryColour() == 1) {
							
							hitsColourButton_8_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 1 && checkFirstTryColour.getTryColour() == 1) {
							
							hitsColourButton_8_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 2 && checkFirstTryColour.getTryColour() == 2) {
							
							hitsColourButton_8_1.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 2 && checkFirstTryColour.getTryColour() == 2) {
							
							hitsColourButton_8_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 2 && checkFirstTryColour.getTryColour() == 2) {
							
							hitsColourButton_8_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 2 && checkFirstTryColour.getTryColour() == 2) {
							
							hitsColourButton_8_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 3 && checkFirstTryColour.getTryColour() == 3) {
							
							hitsColourButton_8_1.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 3 && checkFirstTryColour.getTryColour() == 3) {
							
							hitsColourButton_8_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 3 && checkFirstTryColour.getTryColour() == 3) {
							
							hitsColourButton_8_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 3 && checkFirstTryColour.getTryColour() == 3) {
							
							hitsColourButton_8_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 4 && checkFirstTryColour.getTryColour() == 4) {
							
							hitsColourButton_8_1.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 4 && checkFirstTryColour.getTryColour() == 4) {
							
							hitsColourButton_8_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 4 && checkFirstTryColour.getTryColour() == 4) {
							
							hitsColourButton_8_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 4 && checkFirstTryColour.getTryColour() == 4) {
							
							hitsColourButton_8_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 5 && checkFirstTryColour.getTryColour() == 5) {
							
							hitsColourButton_8_1.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 5 && checkFirstTryColour.getTryColour() == 5) {
							
							hitsColourButton_8_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 5 && checkFirstTryColour.getTryColour() == 5) {
							
							hitsColourButton_8_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 5 && checkFirstTryColour.getTryColour() == 5) {
							
							hitsColourButton_8_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 6 && checkFirstTryColour.getTryColour() == 6) {
							
							hitsColourButton_8_1.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 6 && checkFirstTryColour.getTryColour() == 6) {
							
							hitsColourButton_8_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 6 && checkFirstTryColour.getTryColour() == 6) {
							
							hitsColourButton_8_1.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 6 && checkFirstTryColour.getTryColour() == 6) {
							
							hitsColourButton_8_1.setBackground(Color.WHITE);
							countBlows++;
							
						}
						
						if (checkSecondCombinationColour.getCombinationColour() == 1 && checkSecondTryColour.getTryColour() == 1) {
							
							hitsColourButton_8_2.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 1 && checkSecondTryColour.getTryColour() == 1) {
							
							hitsColourButton_8_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 1 && checkSecondTryColour.getTryColour() == 1) {
							
							hitsColourButton_8_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 1 && checkSecondTryColour.getTryColour() == 1) {
							
							hitsColourButton_8_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 2 && checkSecondTryColour.getTryColour() == 2) {
							
							hitsColourButton_8_2.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 2 && checkSecondTryColour.getTryColour() == 2) {
							
							hitsColourButton_8_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 2 && checkSecondTryColour.getTryColour() == 2) {
							
							hitsColourButton_8_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 2 && checkSecondTryColour.getTryColour() == 2) {
							
							hitsColourButton_8_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 3 && checkSecondTryColour.getTryColour() == 3) {
							
							hitsColourButton_8_2.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 3 && checkSecondTryColour.getTryColour() == 3) {
							
							hitsColourButton_8_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 3 && checkSecondTryColour.getTryColour() == 3) {
							
							hitsColourButton_8_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 3 && checkSecondTryColour.getTryColour() == 3) {
							
							hitsColourButton_8_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 4 && checkSecondTryColour.getTryColour() == 4) {
							
							hitsColourButton_8_2.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 4 && checkSecondTryColour.getTryColour() == 4) {
							
							hitsColourButton_8_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 4 && checkSecondTryColour.getTryColour() == 4) {
							
							hitsColourButton_8_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 4 && checkSecondTryColour.getTryColour() == 4) {
							
							hitsColourButton_8_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 5 && checkSecondTryColour.getTryColour() == 5) {
							
							hitsColourButton_8_2.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 5 && checkSecondTryColour.getTryColour() == 5) {
							
							hitsColourButton_8_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 5 && checkSecondTryColour.getTryColour() == 5) {
							
							hitsColourButton_8_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 5 && checkSecondTryColour.getTryColour() == 5) {
							
							hitsColourButton_8_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 6 && checkSecondTryColour.getTryColour() == 6) {
							
							hitsColourButton_8_2.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 6 && checkSecondTryColour.getTryColour() == 6) {
							
							hitsColourButton_8_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 6 && checkSecondTryColour.getTryColour() == 6) {
							
							hitsColourButton_8_2.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 6 && checkSecondTryColour.getTryColour() == 6) {
							
							hitsColourButton_8_2.setBackground(Color.WHITE);
							countBlows++;
							
						}
						
						if (checkThirdCombinationColour.getCombinationColour() == 1 && checkThirdTryColour.getTryColour() == 1) {
							
							hitsColourButton_8_3.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 1 && checkThirdTryColour.getTryColour() == 1) {
							
							hitsColourButton_8_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 1 && checkThirdTryColour.getTryColour() == 1) {
							
							hitsColourButton_8_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 1 && checkThirdTryColour.getTryColour() == 1) {
							
							hitsColourButton_8_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 2 && checkThirdTryColour.getTryColour() == 2) {
							
							hitsColourButton_8_3.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 2 && checkThirdTryColour.getTryColour() == 2) {
							
							hitsColourButton_8_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 2 && checkThirdTryColour.getTryColour() == 2) {
							
							hitsColourButton_8_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 2 && checkThirdTryColour.getTryColour() == 2) {
							
							hitsColourButton_8_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 3 && checkThirdTryColour.getTryColour() == 3) {
							
							hitsColourButton_8_3.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 3 && checkThirdTryColour.getTryColour() == 3) {
							
							hitsColourButton_8_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 3 && checkThirdTryColour.getTryColour() == 3) {
							
							hitsColourButton_8_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 3 && checkThirdTryColour.getTryColour() == 3) {
							
							hitsColourButton_8_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 4 && checkThirdTryColour.getTryColour() == 4) {
							
							hitsColourButton_8_3.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 4 && checkThirdTryColour.getTryColour() == 4) {
							
							hitsColourButton_8_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 4 && checkThirdTryColour.getTryColour() == 4) {
							
							hitsColourButton_8_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 4 && checkThirdTryColour.getTryColour() == 4) {
							
							hitsColourButton_8_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 5 && checkThirdTryColour.getTryColour() == 5) {
							
							hitsColourButton_8_3.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 5 && checkThirdTryColour.getTryColour() == 5) {
							
							hitsColourButton_8_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 5 && checkThirdTryColour.getTryColour() == 5) {
							
							hitsColourButton_8_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 5 && checkThirdTryColour.getTryColour() == 5) {
							
							hitsColourButton_8_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 6 && checkThirdTryColour.getTryColour() == 6) {
							
							hitsColourButton_8_3.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 6 && checkThirdTryColour.getTryColour() == 6) {
							
							hitsColourButton_8_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 6 && checkThirdTryColour.getTryColour() == 6) {
							
							hitsColourButton_8_3.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 6 && checkThirdTryColour.getTryColour() == 6) {
							
							hitsColourButton_8_3.setBackground(Color.WHITE);
							countBlows++;
							
						}
						
						if (checkFourthCombinationColour.getCombinationColour() == 1 && checkFourthTryColour.getTryColour() == 1) {
							
							hitsColourButton_8_4.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 1 && checkFourthTryColour.getTryColour() == 1) {
							
							hitsColourButton_8_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 1 && checkFourthTryColour.getTryColour() == 1) {
							
							hitsColourButton_8_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 1 && checkFourthTryColour.getTryColour() == 1) {
							
							hitsColourButton_8_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 2 && checkFourthTryColour.getTryColour() == 2) {
							
							hitsColourButton_8_4.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 2 && checkFourthTryColour.getTryColour() == 2) {
							
							hitsColourButton_8_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 2 && checkFourthTryColour.getTryColour() == 2) {
							
							hitsColourButton_8_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 2 && checkFourthTryColour.getTryColour() == 2) {
							
							hitsColourButton_8_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 3 && checkFourthTryColour.getTryColour() == 3) {
							
							hitsColourButton_8_4.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 3 && checkFourthTryColour.getTryColour() == 3) {
							
							hitsColourButton_8_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 3 && checkFourthTryColour.getTryColour() == 3) {
							
							hitsColourButton_8_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 3 && checkFourthTryColour.getTryColour() == 3) {
							
							hitsColourButton_8_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 4 && checkFourthTryColour.getTryColour() == 4) {
							
							hitsColourButton_8_4.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 4 && checkFourthTryColour.getTryColour() == 4) {
							
							hitsColourButton_8_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 4 && checkFourthTryColour.getTryColour() == 4) {
							
							hitsColourButton_8_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 4 && checkFourthTryColour.getTryColour() == 4) {
							
							hitsColourButton_8_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 5 && checkFourthTryColour.getTryColour() == 5) {
							
							hitsColourButton_8_4.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 5 && checkFourthTryColour.getTryColour() == 5) {
							
							hitsColourButton_8_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 5 && checkFourthTryColour.getTryColour() == 5) {
							
							hitsColourButton_8_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 5 && checkFourthTryColour.getTryColour() == 5) {
							
							hitsColourButton_8_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkFourthCombinationColour.getCombinationColour() == 6 && checkFourthTryColour.getTryColour() == 6) {
							
							hitsColourButton_8_4.setBackground(Color.ORANGE);
							countHits++;
							
						} else if (checkFirstCombinationColour.getCombinationColour() == 6 && checkFourthTryColour.getTryColour() == 6) {
							
							hitsColourButton_8_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkSecondCombinationColour.getCombinationColour() == 6 && checkFourthTryColour.getTryColour() == 6) {
							
							hitsColourButton_8_4.setBackground(Color.WHITE);
							countBlows++;
							
						} else if (checkThirdCombinationColour.getCombinationColour() == 6 && checkFourthTryColour.getTryColour() == 6) {
							
							hitsColourButton_8_4.setBackground(Color.WHITE);
							countBlows++;
							
						}
						
						if (countHits == 4) {
							
							JOptionPane.showMessageDialog(null, "You won the game!");
							
						} else {
							
							JOptionPane.showMessageDialog(null, "You lost the game!");
							
						}
						
					}
					
				});
				
			}
			
		});
		
		// Ventana de confirmación que permite al usuario cerrar el programa
		exitMenuItem.addActionListener(new ActionListener() {
			
			public void actionPerformed (ActionEvent e) {
				
				int confirmExit = JOptionPane.showConfirmDialog(null, "Are you sure you want to exit?", "Confirm Exit", JOptionPane.YES_NO_OPTION);
				
				if (confirmExit == JOptionPane.YES_OPTION) {
					
					System.exit(0);
					
				}
				
			}
			
		});
		
		// Ventana de información que explica al jugador como jugar
		howMenuItem.addActionListener(new ActionListener() {
			
			public void actionPerformed (ActionEvent e) {
				
				JOptionPane.showMessageDialog(null, "This menu will show the player how to play.");
				
			}
			
		});
		
		// Ventana que muestra información del programa
		aboutMenuItem.addActionListener(new ActionListener() {
			
			public void actionPerformed (ActionEvent e) {
				
				JOptionPane.showMessageDialog(null, "Copyright (c) 2020, Dapase and/or its affiliates. All rights reserved.");
				
			}
			
		});
		
	}
}