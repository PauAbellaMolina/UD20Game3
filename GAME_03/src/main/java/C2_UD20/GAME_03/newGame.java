package C2_UD20.GAME_03;
import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.SpringLayout;
import javax.swing.border.LineBorder;
import java.awt.Color;
import javax.swing.JButton;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;

public class newGame extends JFrame {

	private JPanel contentPane;
	private final ButtonGroup buttonGroup = new ButtonGroup();

	public static void main(String[] args) {
		
		EventQueue.invokeLater(new Runnable() {
			
			public void run() {
				
				try {
					
					newGame frame = new newGame();
					frame.setVisible(true);
					
				} catch (Exception e) {
					
					e.printStackTrace();
					
				}
				
			}
			
		});
		
	}

	public newGame() {
		setTitle("Choose a difficulty level");
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 320, 320);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		SpringLayout sl_contentPane = new SpringLayout();
		contentPane.setLayout(sl_contentPane);
		
		JPanel panel = new JPanel();
		sl_contentPane.putConstraint(SpringLayout.NORTH, panel, 5, SpringLayout.NORTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.WEST, panel, 5, SpringLayout.WEST, contentPane);
		sl_contentPane.putConstraint(SpringLayout.SOUTH, panel, 196, SpringLayout.NORTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.EAST, panel, 289, SpringLayout.WEST, contentPane);
		panel.setBorder(new LineBorder(Color.LIGHT_GRAY, 1, true));
		contentPane.add(panel);
		
		JButton okButton = new JButton("OK");
		contentPane.add(okButton);
		
		JButton cancelButton = new JButton("Cancel");
		sl_contentPane.putConstraint(SpringLayout.NORTH, okButton, 0, SpringLayout.NORTH, cancelButton);
		sl_contentPane.putConstraint(SpringLayout.EAST, okButton, -6, SpringLayout.WEST, cancelButton);
		sl_contentPane.putConstraint(SpringLayout.SOUTH, cancelButton, -10, SpringLayout.SOUTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.EAST, cancelButton, 0, SpringLayout.EAST, panel);
		
		JRadioButton beginnerRadioButton = new JRadioButton("Beginner");
		buttonGroup.add(beginnerRadioButton);
		panel.add(beginnerRadioButton);
		
		JRadioButton mediumRadioButton = new JRadioButton("Medium");
		buttonGroup.add(mediumRadioButton);
		panel.add(mediumRadioButton);
		
		JRadioButton advancedRadioButton = new JRadioButton("Advanced");
		buttonGroup.add(advancedRadioButton);
		panel.add(advancedRadioButton);
		contentPane.add(cancelButton);
		
		okButton.addActionListener(new ActionListener() {
			
			public void actionPerformed (ActionEvent e) {
				
				JOptionPane.showMessageDialog(null, "This will start the game with the chosen difficulty.");
				
			}
			
		});
		
		cancelButton.addActionListener(new ActionListener() {
			
			public void actionPerformed (ActionEvent e) {
				
				System.exit(0);
				
			}
			
		});
		
	}
	
}
